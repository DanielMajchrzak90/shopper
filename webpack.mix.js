const mix = require('laravel-mix');
var path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
 
mix.webpackConfig({
  resolve: {
    alias: {
      '@':  path.resolve( __dirname, 'resources/js')
    }
  },
  module: {
    rules: [
      {
        // Matches all PHP or JSON files in `resources/lang` directory.
        test: /resources[\\\/]lang.+\.(php|json)$/,
        loader: 'laravel-localization-loader',
      }
    ]
  }
});

mix.js('resources/js/admin/app.js', 'public/js/admin').vue()
  .js('resources/js/shop/app.js', 'public/js/shop').vue()
  .js('resources/js/client/app.js', 'public/js/client').vue()
  .sass('resources/sass/admin/app.scss', 'public/css/admin')
  .sass('resources/sass/shop/app.scss', 'public/css/shop')
  .sass('resources/sass/client/app.scss', 'public/css/client')
  .sass('resources/sass/app.scss', 'public/css')
  .version();