<?php

use App\Http\Controllers\Ajax\AuthController;
use App\Http\Controllers\Ajax\Panel;

Route::group([
    
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('forgot-password', [AuthController::class, 'forgotPassword']);
    Route::post('reset-password', [AuthController::class, 'resetPassword']);
    Route::post('me', [AuthController::class, 'me']);

});

Route::group([
    'prefix' => 'panel',
    'middleware' => ['auth:ajax']
], function() {
    Route::resource('attributes', Panel\AttributesController::class);
    Route::resource('attribute-groups', Panel\AttributeGroupsController::class);
    Route::resource('locales', Panel\LocalesController::class);
    Route::resource('categories', Panel\CategoriesController::class);
    Route::resource('menu-items', Panel\MenuItemsController::class);
    Route::resource('products', Panel\ProductsController::class);

    Route::get('attributes-by-category/{category}', Panel\AttributesByCategoryView::class);

    Route::get('categories-tree', [Panel\CategoriesTreeController::class, 'index']);
    Route::patch('categories-tree', [Panel\CategoriesTreeController::class, 'update']);
    Route::get('enums', Panel\EnumsView::class);

    Route::group([
        'prefix' => 'selects',
        'middleware' => ['trueFallbackLocale'],
    ], function() {
        Route::get('attributes', Panel\Selects\AttributesView::class);
        Route::get('attribute-groups', Panel\Selects\AttributeGroupsView::class);
        Route::get('menu-items', Panel\Selects\MenuItemsView::class);
        Route::get('categories/{category?}', Panel\Selects\CategoryView::class);
        Route::get('products', Panel\Selects\ProductsView::class);
    });
});