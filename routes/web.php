<?php

use Illuminate\Routing\Router ;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Http\Controllers\Shop\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware' => ['trueFallbackLocale'],
], function() {
    Route::get('/', function () {
        return view('dashboard');
    });
});

$dashboardView = function () {
    return view('admin.dashboard');
};

Route::prefix('admin')
    ->group(function() use ($dashboardView) {
        Route::get('/login', $dashboardView)->name('login');
        Route::get('/password-reset', $dashboardView)->name('password.reset');
    });

Route::get('/{slug?}', [DashboardController::class, 'index'])
    ->where('slug', '.*')
    ->name('shop');