<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class AttributeType extends Enum implements LocalizedEnum
{
    const TEXT_TYPE = \ACME\AttributeTypes\TextType::class;
    const BOOL_TYPE = \ACME\AttributeTypes\BooleanType::class;
    const SELECT_TYPE = \ACME\AttributeTypes\SelectType::class;
    const COLOR_TYPE = \ACME\AttributeTypes\ColorType::class;
    const NUMBER_TYPE = \ACME\AttributeTypes\NumberType::class;

    public static function getAttributeTypesArray()
    {
        return self::getAttributeTypes()->map(fn($type) => $type->toArray())->toArray();
    }

    public static function getAttributeTypes()
    {
       $types = self::getValues();

        return collect($types)->mapWithKeys(fn($type) => [$type => new $type]); 
    }

    public function toArray()
    {
        return $this;
    }
}
