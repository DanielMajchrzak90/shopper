<?php 

namespace ACME;

use Illuminate\Foundation\Application as BaseApplication;

class Application extends BaseApplication
{
   	public function setContentLocale($locale)
    {
        $this['config']->set('app.content_locale', $locale);
    }

    public function getContentLocale()
    {
        return $this['config']->get('app.content_locale');
    }
}