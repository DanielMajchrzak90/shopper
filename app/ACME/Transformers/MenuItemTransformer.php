<?php

namespace ACME\Transformers;

use App\Models\MenuItem;

class MenuItemTransformer extends Transformer
{
	protected $model;

	public function __construct(MenuItem $menuItem)
	{
		$this->model = $menuItem;
	}

	public function cols() 
	{
		return $this->getModel()
			->cols
           	->map(function($col) {;
				$data = $col->only(['id', 'name', 'rid']);
				$data['items'] = $col->items->pluck('id')->toArray();

				return $data;
			})->toArray();
	}

	public function toArray()
	{
		$menuItem = $this->getModel();

		$data = $menuItem->only([
			'id', 'name', 'slug', 'category_id', 'image_link', 'image_title', 'image_url'
		]);
		$data['cols'] = $this->cols();

		return $data;
	}
}