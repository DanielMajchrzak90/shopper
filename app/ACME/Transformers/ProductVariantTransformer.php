<?php

namespace ACME\Transformers;

use App\Models\Product;
use App;

class ProductVariantTransformer extends Transformer
{
	protected $model;

	public function __construct(Product $product)
	{
		$this->model = $product;
	}

	public function toArray()
	{
		$product = $this->getModel();

		$data = $product->only([
			'id', 
			'price',
			'quantity',
			'rid',
		]);

		foreach ($product->attributesValues as $value) {
			$data[$value->attribute->name] = $value->attribute
				->class_type
				->getValue($value);
		}

		return $data;
	}
}