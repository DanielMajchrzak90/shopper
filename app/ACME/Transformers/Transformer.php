<?php

namespace ACME\Transformers;

use App\Models\Attribute;

abstract class Transformer
{
	protected $model;
	protected $locale;
	protected $fallback = false;

	public function getModel()
	{
		return $this->model;
	}

	public function setLocale(string $locale)
	{
		$this->locale = $locale;

		return $this;
	}

	public function setFallback(bool $fallback)
	{
		$this->fallback = $fallback;

		return $this;
	}

	public function getLocale()
	{
		return $this->locale ?: \App::getLocale();
	}

	public function getFallback()
	{
		return $this->fallback;
	}
}