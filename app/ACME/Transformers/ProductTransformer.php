<?php

namespace ACME\Transformers;

use App\Models\Product;
use App;

class ProductTransformer extends Transformer
{
	protected $model;

	public function __construct(Product $product)
	{
		$this->model = $product;
	}

	public function getVariants() 
	{
		return $this->getModel()
			->variants
           	->map(function($variant) {
				return (new ProductVariantTransformer($variant))->toArray();
			})->toArray();
	}

	public function toArray()
	{
		$product = $this->getModel();

		$data = $product->only([
			'id', 
			'name', 
			'slug', 
			'category_id', 
			'sku', 
			'description',
			'is_active',
			'price',
			'quantity',
			'images',
			'related_products',
			'locale',
		]);

		foreach ($product->attributesValues as $value) {
			$data[$value->attribute->name] = $value->attribute
				->class_type
				->getValue($value);
		}

		$data['variants'] = $this->getVariants();

		return $data;
	}
}