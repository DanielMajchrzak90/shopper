<?php

namespace ACME\Transformers;

use App\Models\Category;

class CategoryTransformer extends Transformer
{
	protected $model;

	public function __construct(Category $category)
	{
		$this->model = $category;
	}

	public function toArray()
	{
		$category = $this->getModel();

		return $category;
	}
}