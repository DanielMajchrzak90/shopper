<?php

namespace ACME\Transformers;

use App\Models\AttributeGroup;

class AttributeGroupTransformer extends Transformer
{
	protected $model;

	public function __construct(AttributeGroup $group)
	{
		$this->model = $group;
	}

	public function toArray()
	{
		$group = $this->getModel();
        $group->attributes->transform(function($attribute) {
            return $attribute->id;
        });

		return $group->only(['id', 'name', 'attributes']);
	}
}