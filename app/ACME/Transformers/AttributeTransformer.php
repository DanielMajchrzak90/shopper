<?php

namespace ACME\Transformers;

use App\Models\Attribute;

class AttributeTransformer extends Transformer
{
	protected $model;

	public function __construct(Attribute $attribute)
	{
		$this->model = $attribute;
	}

	public function toArray()
	{
		$attribute = $this->getModel();

		return array_merge(
			$attribute->only(['id', 'name', 'label', 'type', 'is_variant']),
			[
				'options' => $attribute->options->map(function($option) {
					return $option->only('id', 'name', 'additional', 'rid');
				})
			]
		);
	}
}