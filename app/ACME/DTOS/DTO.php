<?php 

namespace ACME\DTOS;

use Arr;

abstract class DTO
{
	protected $data;
	protected $locale;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function setLocale(string $locale)
	{
		$this->locale = $locale;

		return $this;
	}

	public function getLocale()
	{
		return $this->locale ?: \App::getContentLocale();
	}

	public function isNew()
	{
		return !$this->getId();
	}

	public function getId()
	{
		return Arr::get($this->data, 'id');
	}

	abstract public function toArray() : array;
}