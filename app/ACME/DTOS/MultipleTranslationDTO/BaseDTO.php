<?php 

namespace ACME\DTOS\MultipleTranslationDTO;

use Arr;
use ACME\DTOS\DTO;

abstract class BaseDTO extends DTO
{
	public function toArray() : array
	{
		$data = $this->basicData();
		$locales = config('translatable.locales');

		foreach ($locales as $locale) {
			$data[$locale] = $this->translationData($locale);
		}
		return $data;
	}

	abstract protected function basicData() : array;

	abstract protected function translationData($locale) : array;
}