<?php 

namespace ACME\DTOS\MultipleTranslationDTO;

use Arr;

class AttributeOptionDTO extends BaseDTO
{
	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'additional' => Arr::get($data, 'additional'),
		];
	}

	protected function translationData($locale) : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, $locale.'.name'),
		];
	}
}