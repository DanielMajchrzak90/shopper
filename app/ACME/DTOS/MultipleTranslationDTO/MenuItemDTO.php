<?php 

namespace ACME\DTOS\MultipleTranslationDTO;

use Arr;
use Illuminate\Support\Collection;

class MenuItemDTO extends BaseDTO
{
	public function getCols()
	{
		$data = $this->data;
		$cols = collect(Arr::get($data, 'cols'));

		$cols = $cols->map(function($col) {
			return (new MenuItemColDTO($col));
		});

		return $cols;
	}

	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'category_id' => Arr::get($data, 'category_id'),
		];
	}

	protected function translationData($locale) : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, $locale.'.name'),
			'slug' => Arr::get($data, $locale.'.slug'),
		];
	}
}