<?php 

namespace ACME\DTOS\MultipleTranslationDTO;

use Arr;

class CategoryDTO extends BaseDTO
{
	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'parent_id' => Arr::get($data, 'parent_id'),
			'group_id' => Arr::get($data, 'group_id'),
		];
	}

	protected function translationData($locale) : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, $locale.'.name'),
			'slug' => Arr::get($data, $locale.'.slug'),
		];
	}
}