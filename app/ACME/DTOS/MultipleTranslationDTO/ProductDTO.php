<?php 

namespace ACME\DTOS\MultipleTranslationDTO;

use Arr;

class ProductDTO extends BaseDTO
{
	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'category_id' => Arr::get($data, 'category_id'),
			'sku' => Arr::get($data, 'sku'),
			'is_active' => Arr::get($data, 'is_active'),
			'quantity' => Arr::get($data, 'quantity'),
			'price' => Arr::get($data, 'price'),
		];
	}

	protected function translationData($locale) : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, $locale.'.name'),
			'slug' => Arr::get($data, $locale.'.slug'),
			'description' => Arr::get($data, $locale.'.description'),
		];
	}
}