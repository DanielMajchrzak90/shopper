<?php 

namespace ACME\DTOS\MultipleTranslationDTO;

use Arr;

class MenuItemColDTO extends BaseDTO
{
	public function getItems()
	{
		$data = $this->data; 

		return Arr::get($data, 'items');
	}

	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'category_id' => Arr::get($data, 'category_id'),
		];
	}

	protected function translationData($locale) : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, $locale.'.name'),
			'slug' => Arr::get($data, $locale.'.slug'),
		];
	}
}