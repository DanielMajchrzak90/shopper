<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;

class MenuItemColDTO extends BaseDTO
{
	public function getItems()
	{
		$data = $this->data; 

		return Arr::get($data, 'items');
	}
	
	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'category_id' => Arr::get($data, 'category_id'),
		];
	}

	protected function translationData() : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, 'name'),
			'slug' => Arr::get($data, 'slug'),
		];
	}
}