<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;
use Illuminate\Support\Collection;
use App\Models\Category;

class ProductVariantDTO extends BaseDTO
{
	public function getAttributes(Category $category) : Collection
	{
		$attributes = $category->variantAttributes()->map(function($attribute) {
			return (new ProductAttributeValueDTO([
				$attribute->class_type->column() => Arr::get($this->data, $attribute->name)
			]))->setAttribute($attribute);
		});

		return $attributes;
	}

	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'quantity' => Arr::get($data, 'quantity'),
			'price' => Arr::get($data, 'price'),
		];
	}

	protected function translationData() : array
	{
		return [];
	}
}