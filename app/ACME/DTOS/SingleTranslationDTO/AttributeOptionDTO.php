<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;

class AttributeOptionDTO extends BaseDTO
{
	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'additional' => Arr::get($data, 'additional'),
		];
	}

	protected function translationData() : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, 'name'),
		];
	}
}