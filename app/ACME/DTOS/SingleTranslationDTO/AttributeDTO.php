<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;
use Illuminate\Support\Collection;

class AttributeDTO extends BaseDTO
{
	public function getOptions() : Collection
	{
		$data = $this->data;
		$options = collect(Arr::get($data, 'options'));

		$options = $options->map(function($opt) {
			return (new AttributeOptionDTO($opt));
		});

		return $options;
	}

	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'type' => Arr::get($data, 'type'),
			'name' => Arr::get($data, 'name'),
			'is_variant' => Arr::get($data, 'is_variant'),
		];
	}

	protected function translationData() : array
	{
		$data = $this->data;

		return [
			'label' => Arr::get($data, 'label'),
		];
	}
}