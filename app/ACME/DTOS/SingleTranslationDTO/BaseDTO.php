<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;
use ACME\DTOS\DTO;

abstract class BaseDTO extends DTO
{
	public function toArray() : array
	{
		return array_merge(
			$this->basicData(),
			[$this->getLocale() => $this->translationData()]
		);
	}

	abstract protected function basicData() : array;

	protected function translationData() : array
	{
		return [];
	}
}