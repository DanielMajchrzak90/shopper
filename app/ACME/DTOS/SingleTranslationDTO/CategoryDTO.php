<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;

class CategoryDTO extends BaseDTO
{
	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'parent_id' => Arr::get($data, 'parent_id'),
			'group_id' => Arr::get($data, 'group_id'),
		];
	}

	protected function translationData() : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, 'name'),
			'slug' => Arr::get($data, 'slug'),
		];
	}
}