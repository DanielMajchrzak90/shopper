<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;
use Illuminate\Support\Collection;
use App\Models\Category;

class ProductDTO extends BaseDTO
{
	public function getAttributes(Category $category) : Collection
	{
		$attributes = $category->attributes()->map(function($attribute) {
			return (new ProductAttributeValueDTO([
				$attribute->class_type->column() => Arr::get($this->data, $attribute->name)
			]))->setAttribute($attribute);
		});

		return $attributes;
	}

	public function getVariants() : Collection
	{		
		$data = $this->data;
		$variants = collect(Arr::get($data, 'variants'));

		dd($variants);

		$variants = $variants->map(function($variant) {
			return (new ProductVariantDTO($variant));
		});

		return $variants;		
	}

	public function getRelatedProducts() : array
	{		
		$data = $this->data;

		return Arr::get($data, 'related_products') ?: [];		
	}

	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'category_id' => Arr::get($data, 'category_id'),
			'sku' => Arr::get($data, 'sku'),
			'is_active' => Arr::get($data, 'is_active'),
			'quantity' => Arr::get($data, 'quantity'),
			'price' => Arr::get($data, 'price'),
		];
	}

	protected function translationData() : array
	{
		$data = $this->data;

		return [
			'name' => Arr::get($data, 'name'),
			'slug' => Arr::get($data, 'slug'),
			'description' => Arr::get($data, 'description'),
		];
	}
}