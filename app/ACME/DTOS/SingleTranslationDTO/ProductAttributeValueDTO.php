<?php 

namespace ACME\DTOS\SingleTranslationDTO;

use Arr;
use Illuminate\Support\Collection;
use App\Models\Attribute;

class ProductAttributeValueDTO extends BaseDTO
{
	protected $attribute;

	public function setAttribute(Attribute $attribute)
	{
		$this->attribute = $attribute;

		return $this;
	}

	public function getAttribute()
	{
		return $this->attribute;
	}

	protected function basicData() : array
	{
		$data = $this->data;

		return [
			'attribute_option_value' => Arr::get($data, 'attribute_option_value'),
			'bool_value' => Arr::get($data, 'bool_value'),
			'num_value' => Arr::get($data, 'num_value'),
		];
	}

	protected function translationData() : array
	{
		$data = $this->data;

		return array_filter([
			'text_value' => Arr::get($data, 'text_value'),
		]);
	}
}