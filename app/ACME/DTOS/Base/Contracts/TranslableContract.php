<?php 

namespace ACME\DTOS\Base\Contracts;

interface TranslableContract
{
	public function getTranslations() : array;
	public function getTranslationsToArray() : array;
}