<?php 

namespace ACME\DTOS\Base\Contracts;

interface TranslateContract
{
	public function setLocale(string $locale) : self;
	public function getLocale() : string;
}