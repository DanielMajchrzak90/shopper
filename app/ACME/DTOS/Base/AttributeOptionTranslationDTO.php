<?php 

namespace ACME\DTOS\Base;

class AttributeOptionTranslationDTO implements Contracts\TranslateContract
{
	use Traits\LocaleTrait;

	protected $name;

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function toArray() : array
	{
		return [
			'name' => $this->getName(),
		];
	}
}