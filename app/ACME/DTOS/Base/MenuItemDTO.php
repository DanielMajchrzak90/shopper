<?php 

namespace ACME\DTOS\Base;

use Symfony\Component\HttpFoundation\File\File;

class MenuItemDTO implements Contracts\TranslableContract
{
	use Traits\TranslableTrait;

	protected $imageLink;
	protected $categoryId;
	protected $query;
	protected $position;
	protected $cols = [];
	protected $image = [];

	public function setImage(?File $image) : self
	{
		$this->image = $image;

		return $this;
	}

	public function getImage(): ?File
	{
		return $this->image;
	}

	public function setImageLink(?string $imageLink): self
	{
		$this->imageLink = $imageLink;

		return $this;
	}

	public function getImageLink(): ?string
	{
		return $this->imageLink;
	}

	public function setCategoryId(?int $categoryId): self
	{
		$this->categoryId = $categoryId;

		return $this;
	}

	public function getCategoryId(): ?int
	{
		return $this->categoryId;
	}

	public function setQuery(?string $query): self
	{
		$this->query = $query;

		return $this;
	}

	public function getQuery(): ?string
	{
		return $this->query;
	}

	public function setPosition(?int $position) : self
	{
		$this->position = $position;

		return $this;
	}

	public function getPosition(): ?int
	{
		return $this->position;
	}

	public function addTranslation(MenuItemTranslationDTO $translation) : self
	{
		$this->translations[] = $translation;

		return $this;
	}

	public function addCol(MenuItemColDTO $col) : self
	{
		$this->cols[] = $col;

		return $this;
	}

	public function getCols(): array
	{
		return $this->cols;
	}

	public function getDataToArray() : array
	{
		return [
			'category_id' => $this->getCategoryId(),
			'query' => $this->getQuery(),
			'position' => $this->getPosition(),
			'image_link' => $this->getImageLink(),
		];
	}
}