<?php 

namespace ACME\DTOS\Base\Traits;

trait TranslableTrait
{
	protected $translations = [];

	public function getTranslations(): array
	{
		return $this->translations;
	}

	public function getTranslationsToArray() : array
	{
		$data = [];

		foreach ($this->getTranslations() as $translation) {
			$data[$translation->getLocale()] = $translation->toArray();
		}

		return $data;
	}

	public function toArray() : array
	{
		return array_merge(
			$this->getDataToArray(),
			$this->getTranslationsToArray()
		);
	}
}