<?php 

namespace ACME\DTOS\Base\Traits;

trait IDTrait
{
	protected $id;

	public function isNew()
	{
		return !$this->getId();
	}

	public function setId(?int $id): self
	{
		$this->id = $id;

		return $this;
	}

	public function getId(): ?int
	{
		return $this->id;
	}
}