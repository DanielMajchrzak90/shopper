<?php 

namespace ACME\DTOS\Base\Traits;

trait LocaleTrait
{
	protected $locale;

	public function setLocale(string $locale) : self
	{
		$this->locale = $locale;

		return $this;
	}

	public function getLocale(): string
	{
		return $this->locale;
	}
}