<?php 

namespace ACME\DTOS\Base;

class MenuItemTranslationDTO implements Contracts\TranslateContract
{
	use Traits\LocaleTrait;

	protected $name;
	protected $slug;
	protected $imageTitle;

	public function setName(?string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setSlug(?string $slug): self
	{
		$this->slug = $slug;

		return $this;
	}

	public function getSlug(): ?string
	{
		return $this->slug;
	}

	public function setImageTitle(?string $imageTitle): self
	{
		$this->imageTitle = $imageTitle;

		return $this;
	}

	public function getImageTitle(): ?string
	{
		return $this->imageTitle;
	}

	public function toArray()
	{
		return [
			'name' => $this->getName(),
			'slug' => $this->getSlug(),
			'image_title' => $this->getImageTitle(),
		];
	}
}