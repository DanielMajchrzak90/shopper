<?php 

namespace ACME\DTOS\Base;

class CategoryTranslationDTO implements Contracts\TranslateContract
{
	use Traits\LocaleTrait;

	protected $name;
	protected $slug;

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setSlug(string $slug): self
	{
		$this->slug = $slug;

		return $this;
	}

	public function getSlug(): string
	{
		return $this->slug;
	}

	public function toArray() : array
	{
		return [
			'name' => $this->getName(),
			'slug' => $this->getSlug(),
		];
	}
}