<?php 

namespace ACME\DTOS\Base;

class ProductImageDTO
{
	use Traits\IDTrait; 

	protected $filename;

	public function setFilename(string $filename): self
	{
		$this->filename = $filename;

		return $this;
	}

	public function getFilename(): string
	{
		return $this->filename;
	}
}