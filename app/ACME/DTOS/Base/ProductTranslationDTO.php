<?php 

namespace ACME\DTOS\Base;

class ProductTranslationDTO implements Contracts\TranslateContract
{
	use Traits\LocaleTrait;

	protected $name;
	protected $slug;
	protected $description;
	
	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setSlug(string $slug): self
	{
		$this->slug = $slug;

		return $this;
	}

	public function getSlug(): string
	{
		return $this->slug;
	}

	public function setDescription(string $description): self
	{
		$this->description = $description;

		return $this;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function toArray()
	{
		return [
			'name' => $this->getName(),
			'slug' => $this->getSlug(),
			'description' => $this->getDescription(),
		];
	}
}