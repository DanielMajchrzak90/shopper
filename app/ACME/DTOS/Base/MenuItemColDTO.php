<?php 

namespace ACME\DTOS\Base;

class MenuItemColDTO implements Contracts\TranslableContract
{
	use Traits\IDTrait, Traits\TranslableTrait;

	protected $items = [];

	public function addTranslation(MenuItemColTranslationDTO $translation) : self
	{
		$this->translations[] = $translation;

		return $this;
	}

	public function addItem(int $id) : self
	{
		$this->items[] = $id;

		return $this;
	}

	public function getItems(): array
	{
		return $this->items;
	}

	public function getDataToArray() : array
	{
		return [];
	}
}