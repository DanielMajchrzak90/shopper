<?php 

namespace ACME\DTOS\Base;

class ProductVariantDTO
{
	use Traits\IDTrait;

	protected $price;
	protected $quantity;
	protected $attributes = [];

	public function setPrice(float $price): self
	{
		$this->price = $price;

		return $this;
	}

	public function getPrice(): float
	{
		return $this->price;
	}

	public function setQuantity(int $quantity): self
	{
		$this->quantity = $quantity;

		return $this;
	}

	public function getQuantity(): int
	{
		return $this->quantity;
	}

	public function addAttribute(ProductAttributeValueDTO $attribute) : self
	{
		$this->attributes[] = $attribute;

		return $this;
	}

	public function getAttributes(): array
	{
		return $this->attributes;
	}

	public function toArray() : array
	{
		return [
			'quantity' => $this->getQuantity(),
			'price' => $this->getPrice(),
		];
	}
}