<?php 

namespace ACME\DTOS\Base;

class AttributeTranslationDTO implements Contracts\TranslateContract
{
	use Traits\LocaleTrait;

	protected $label;

	public function setLabel(string $label): self
	{
		$this->label = $label;

		return $this;
	}

	public function getLabel(): string
	{
		return $this->label;
	}

	public function toArray() : array
	{
		return [
			'label' => $this->getLabel(),
		];
	}
}