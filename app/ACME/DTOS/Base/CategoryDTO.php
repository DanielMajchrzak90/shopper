<?php 

namespace ACME\DTOS\Base;

class CategoryDTO implements Contracts\TranslableContract
{
	use Traits\IDTrait, Traits\TranslableTrait;

	protected $parentId;
	protected $groupId;

	public function setParentId(?int $parentId): self
	{
		$this->parentId = $parentId;

		return $this;
	}

	public function getParentId(): ?int
	{
		return $this->parentId;
	}

	public function setGroupId(?int $groupId): self
	{
		$this->groupId = $groupId;

		return $this;
	}

	public function getGroupId(): ?int
	{
		return $this->groupId;
	}

	public function addTranslation(CategoryTranslationDTO $translation) : self
	{
		$this->translations[] = $translation;

		return $this;
	}

	public function getDatatoArray() : array
	{
		return [
			'parent_id' => $this->getParentId(),
			'group_id' => $this->getGroupId(),
		];
	}
}