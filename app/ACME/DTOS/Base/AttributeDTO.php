<?php 

namespace ACME\DTOS\Base;

class AttributeDTO implements Contracts\TranslableContract
{
	use Traits\IDTrait, Traits\TranslableTrait;

	protected $name;
	protected $type;
	protected $isVariant;
	protected $options = [];

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setType(string $type): self
	{
		$this->type = $type;

		return $this;
	}

	public function getType(): string
	{
		return $this->type;
	}

	public function setIsVariant(bool $isVariant): self
	{
		$this->isVariant = $isVariant;

		return $this;
	}

	public function getIsVariant(): bool
	{
		return $this->isVariant;
	}

	public function addOption(AttributeOptionDTO $option): self
	{
		$this->options[] = $option;

		return $this;
	}

	public function getOptions(): array
	{
		return $this->options;
	}

	public function addTranslation(AttributeTranslationDTO $translation) : self
	{
		$this->translations[] = $translation;

		return $this;
	}

	public function getDatatoArray() : array
	{
		return [
			'name' => $this->getName(),
			'type' => $this->getType(),
			'is_variant' => $this->getIsVariant(),
		];
	}
}