<?php 

namespace ACME\DTOS\Base;

class AttributeOptionDTO implements Contracts\TranslableContract
{
	use Traits\IDTrait, Traits\TranslableTrait;

	protected $additional;

	public function setAdditional(?array $additional): self
	{
		$this->additional = $additional;

		return $this;
	}

	public function getAdditional(): ?array
	{
		return $this->additional;
	}

	public function addTranslation(AttributeOptionTranslationDTO $translation) : self
	{
		$this->translations[] = $translation;

		return $this;
	}

	public function getDatatoArray() : array
	{
		return [
			'additional' => $this->getAdditional(),
		];
	}
}