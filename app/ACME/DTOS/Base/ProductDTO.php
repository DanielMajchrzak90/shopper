<?php 

namespace ACME\DTOS\Base;

use App\Models\Category;
use Arr;
use Symfony\Component\HttpFoundation\File\File;

class ProductDTO implements Contracts\TranslableContract
{
	use Traits\IDTrait, Traits\TranslableTrait;

	protected $categoryId;
	protected $sku;
	protected $price;
	protected $quantity;
	protected $isActive;
	protected $productId;
	protected $productsBindingId;
	protected $variants = [];
	protected $translations = [];
	protected $images = [];
	protected $deletedImages = [];
	protected $relatedProducts = [];

	public function setCategoryId(?int $categoryId): self
	{
		$this->categoryId = $categoryId;

		return $this;
	}

	public function getCategoryId(): ?int
	{
		return $this->categoryId;
	}

	public function setSku(string $sku): self
	{
		$this->sku = $sku;

		return $this;
	}

	public function getSku(): string
	{
		return $this->sku;
	}

	public function setPrice(float $price): self
	{
		$this->price = $price;

		return $this;
	}

	public function getPrice(): float
	{
		return $this->price;
	}

	public function setQuantity(int $quantity): self
	{
		$this->quantity = $quantity;

		return $this;
	}

	public function getQuantity(): int
	{
		return $this->quantity;
	}

	public function setIsActive(bool $isActive): self
	{
		$this->isActive = $isActive;

		return $this;
	}

	public function getIsActive(): bool
	{
		return $this->isActive;
	}

	public function setProductId(?int $productId): self
	{
		$this->productId = $productId;

		return $this;
	}

	public function getetProductId(): ?int
	{
		return $this->productId;
	}

	public function setProductsBindingId(?int $productsBindingId): self
	{
		$this->productsBindingId = $productsBindingId;

		return $this;
	}

	public function getProductsBindingId(): ?int
	{
		return $this->productsBindingId;
	}

	public function addTranslation(ProductTranslationDTO $translation) : self
	{
		$this->translations[] = $translation;

		return $this;
	}

	public function addVariant(ProductVariantDTO $variant) : self
	{
		$this->variants[] = $variant;

		return $this;
	}

	public function getVariants(): array
	{
		return $this->variants;
	}

	public function addImage(File $file) : self
	{
		$this->images[] = $file;

		return $this;
	}

	public function getImages(): array
	{
		return $this->images;
	}

	public function addAttribute(ProductAttributeValueDTO $attribute) : self
	{
		$this->attributes[] = $attribute;

		return $this;
	}

	public function getAttributes(): array
	{
		return $this->attributes;
	}

	public function addRelatedProduct(int $id) : self
	{
		$this->relatedProducts[] = $id;

		return $this;
	}

	public function getRelatedProducts(): array
	{
		return $this->relatedProducts;
	}

	public function addDeletedImage(int $id) : self
	{
		$this->deletedImages[] = $id;

		return $this;
	}

	public function getDeletedImages(): array
	{
		return $this->deletedImages;
	}

	public function getDataToArray() : array
	{
		return [
			'category_id' => $this->getCategoryId(),
			'sku' => $this->getSku(),
			'is_active' => $this->getIsActive(),
			'quantity' => $this->getQuantity(),
			'price' => $this->getPrice(),
		];
	}
}