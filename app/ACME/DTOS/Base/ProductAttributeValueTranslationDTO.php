<?php 

namespace ACME\DTOS\Base;

class ProductAttributeValueTranslationDTO implements Contracts\TranslateContract
{
	use Traits\LocaleTrait;

	protected $textValue;

	public function setTextValue(string $textValue): self
	{
		$this->textValue = $textValue;

		return $this;
	}

	public function getTextValue(): ?string
	{
		return $this->textValue;
	}

	public function toArray()
	{
		return [
			'text_value' => $this->getTextValue(),
		];
	}
}