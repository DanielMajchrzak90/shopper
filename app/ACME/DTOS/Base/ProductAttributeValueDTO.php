<?php 

namespace ACME\DTOS\Base;

class ProductAttributeValueDTO implements Contracts\TranslableContract
{
	use Traits\IDTrait, Traits\TranslableTrait;

	protected $productId;
	protected $attributeId;
	protected $optionValue;
	protected $boolValue;
	protected $numValue;
	protected $translations = [];

	public function addTranslation(ProductAttributeValueTranslationDTO $translation) : self
	{
		$this->translations[] = $translation;

		return $this;
	}

	public function setProductId(int $productId): self
	{
		$this->productId = $productId;

		return $this;
	}

	public function getProductId(): int
	{
		return $this->productId;
	}

	public function setAttributeId(int $attributeId): self
	{
		$this->attributeId = $attributeId;

		return $this;
	}

	public function getAttributeId(): int
	{
		return $this->attributeId;
	}

	public function setOptionValue(int $optionValue): self
	{
		$this->optionValue = $optionValue;

		return $this;
	}

	public function getOptionValue(): ?int
	{
		return $this->optionValue;
	}

	public function setBoolValue(bool $boolValue): self
	{
		$this->boolValue = $boolValue;

		return $this;
	}

	public function getBoolValue(): ?bool
	{
		return $this->boolValue;
	}

	public function setNumValue(float $numValue): self
	{
		$this->numValue = $numValue;

		return $this;
	}

	public function getNumValue(): ?float
	{
		return $this->numValue;
	}

	public function getDataToArray()
	{
		return [
			'option_value' => $this->getOptionValue(),
			'bool_value' => $this->getBoolValue(),
			'num_value' => $this->getNumValue(),
		];
	}
}