<?php

namespace ACME\AttributeTypes;

use ACME\DTOS\Base\ProductAttributeValueDTO;
use ACME\DTOS\Base\ProductAttributeValueTranslationDTO;

class TextType extends BaseType
{
	protected $component = 'text-field';
	protected $shopComponent = null;

	public function isTranslable() : bool
	{
		return true;
	}

	public function rules()
	{
		return [
			'required',
			'string',
			'max:255'
		];
	}

	public function column()
	{
		return 'text_value';
	}

	public function transform($value, $locale)
	{
		return [
			$locale => [
				$this->column() => $value
			]
		];
	}

	public function setValue($value)
	{
		return (new ProductAttributeValueDTO)
			->setAttributeOptionValue($value);
		return (new ProductAttributeValueDTO)
            ->addTranslation(
                (new ProductAttributeValueTranslationDTO)
                    ->setLocale(\App::contentLocale())
                    ->setTextValue($value));
	}

	public function fakeData() 
	{		
        return $this->faker->sentence(8);
	}

	public function fakeDTO()
	{	
		$attributeDto = new ProductAttributeValueDTO();
        $attributeDto = $this->setValueDTO(
            $attributeDto, 
            $this->fakeData(),
            'pl'
        );

        $attributeDto = $this->setValueDTO(
            $attributeDto, 
            $this->fakeData(),
            'en'
        );
        
		return $attributeDto;
	}

	public function setValueDTO(ProductAttributeValueDTO $dto, $value, ?string $locale = null) : ProductAttributeValueDTO
	{
		return $dto->addTranslation(
            (new ProductAttributeValueTranslationDTO)
                ->setTextValue($value)
                ->setLocale($locale)
		);
	}
}