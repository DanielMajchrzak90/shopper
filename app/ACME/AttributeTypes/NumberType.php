<?php

namespace ACME\AttributeTypes;

use ACME\DTOS\Base\ProductAttributeValueDTO;

class NumberType extends BaseType
{
	protected $component = 'text-field';
	protected $shopComponent = 'number-field';

	public function rules()
	{
		return [
			'required',
			'numeric'
		];
	}	

	public function column()
	{
		return 'num_value';
	}

	public function transform($value, $locale)
	{
		return [
			$this->column() => $value
		];
	}
	
	public function fakeData() 
	{
		return $this->faker->randomNumber(2);
	}

	public function setValueDTO(ProductAttributeValueDTO $dto, $value, ?string $locale = null) : ProductAttributeValueDTO
	{
		return $dto->setNumValue($value);
	}
}