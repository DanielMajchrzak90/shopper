<?php

namespace ACME\AttributeTypes;

use App\Models\Attribute;
use App\Models\ProductAttributeValue;
use ACME\DTOS\Base\ProductAttributeValueDTO;
use ACME\DTOS\Base\AttributeDTO;

abstract class BaseType
{
	protected $component;
	protected $shopComponent;
	protected $hasOptions = false;
	protected $hasColorOptions = false;
	protected $attribute;
	protected $locale;
	protected $faker;

	public function __construct()
	{
		$this->faker = \Faker\Factory::create();
	}

	public function getComponent() 
	{
		return $this->component;
	}

	public function getShopComponent() 
	{
		return $this->shopComponent;
	}

	public function isTranslable() : bool
	{
		return false;
	}

	public function setAttribute(Attribute $attribute)
	{
		$this->attribute = $attribute;

		return $this;
	}

	abstract public function rules();

	abstract public function transform($value, $locale);

	abstract public function column();

	public function save(AttributeDTO $dto)
	{         
        if (!$attribute = $this->attribute) {
            $attribute = Attribute::create($dto->toArray());
        } else {
            $attribute->update($dto->toArray());
        }
	}

	public function setLocale($locale)
	{
		$this->locale = $locale;
	}

	public function getValue(ProductAttributeValue $value)
	{
		$column = $this->column();

		return $value->$column;
	}

	public function getDescValue(ProductAttributeValue $value)
	{
		return $this->getValue($value);
	}

	public function toArray()
	{
		return [
			'has_options' => $this->hasOptions,
			'has_color_options' => $this->hasColorOptions,
			'component' => $this->component,
		];
	}

	abstract public function fakeData();

	public function fakeDTO()
	{
		return $this->setValueDTO(
            (new ProductAttributeValueDTO()), 
            $this->fakeData()
        );
	}

	abstract public function setValueDTO(
		ProductAttributeValueDTO $dto, 
		$value, 
		?string $locale = null
	) : ProductAttributeValueDTO;
}