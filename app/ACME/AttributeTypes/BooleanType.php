<?php

namespace ACME\AttributeTypes;

use ACME\DTOS\Base\ProductAttributeValueDTO;

class BooleanType extends BaseType
{
	protected $component = 'switcher-field';
	protected $shopComponent = 'bool-field';

	public function rules()
	{
		return [
			'required',
			'boolean',
		];
	}

	public function column()
	{
		return 'bool_value';
	}

	public function transform($value, $locale)
	{
		return [
			$this->column() => $value
		];
	}

	public function fakeData() : array 
	{
		return $this->faker->boolean;
	}

	public function setValueDTO(ProductAttributeValueDTO $dto, $value, ?string $locale = null) : ProductAttributeValueDTO
	{
		return $dto->setBoolValue($value);
	}
}