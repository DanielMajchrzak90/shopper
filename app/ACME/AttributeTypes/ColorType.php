<?php

namespace ACME\AttributeTypes;

use Illuminate\Validation\Rule;

class ColorType extends SelectType
{
	protected $component = 'color-field';
	protected $shopComponent = 'color-field';
	protected $hasOptions = true;
	protected $hasColorOptions = true;
}