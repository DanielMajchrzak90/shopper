<?php

namespace ACME\AttributeTypes;

use Illuminate\Validation\Rule;
use App\Models\Attribute;
use ACME\DTOS\Base\ProductAttributeValueDTO;
use ACME\DTOS\Base\AttributeDTO;
use App\Models\ProductAttributeValue;

class SelectType extends BaseType
{
	protected $component = 'select-field';
	protected $shopComponent = 'select-field';
	protected $hasOptions = true;

	public function rules()
	{
		return [
			'required',
			Rule::in($this->attribute->options->pluck('id')->toArray())
		];
	}

	public function column()
	{
		return 'option_value';
	}
	
	public function transform($value, $locale)
	{
		return [
			$this->column() => $value
		];
	}

	public function save(AttributeDTO $dto)
	{         
        if (!$attribute = $this->attribute) {
            $attribute = Attribute::create($dto->toArray());
        } else {
            $attribute->update($dto->toArray());
        }

        $attribute->saveOptions($dto->getOptions());
	}

	public function setValue($value)
	{
		return (new ProductAttributeValueDTO)
			->setAttributeOptionValue($value);
	}

	public function fakeData() 
	{
		return optional($this->attribute->options->random())->id;
	}

	public function setValueDTO(ProductAttributeValueDTO $dto, $value, ?string $locale = null) : ProductAttributeValueDTO
	{
		return $dto->setOptionValue($value);
	}

	public function getDescValue(ProductAttributeValue $value)
	{
		return $value->option->name;
	}
}