<?php

namespace ACME\Nestedset;

use  Kalnoy\Nestedset\Collection as BaseCollection;
use Illuminate\Database\Eloquent\Collection as EloquentBaseCollection;

class Collection extends BaseCollection
{
    /**
     * Build a tree from a list of nodes. Each item will have set children relation.
     *
     * To successfully build tree "id", "_lft" and "parent_id" keys must present.
     *
     * If `$root` is provided, the tree will contain only descendants of that node.
     *
     * @param mixed $root
     *
     * @return Collection
     */
    public function toTree($root = false)
    {
        $items = [];

        foreach ($this->items as $index => $node) {
            $nextIndex = $index + 1;
            $prevIndex = $index - 1;
            $nextNode = null;

            if (isset($this->items[$nextIndex])) {
                $nextNode = &$this->items[$nextIndex];
            }

            if (optional($nextNode)->isDescendantOf($node)) {
                $this->setRelations($node, $nextNode);
            } else {
                if (isset($this->items[$prevIndex]) && $this->items[$prevIndex]->getParentId()) {
                    $prevtNode = $this->items[$prevIndex];
                    $parent = $prevtNode->getRelation('parent');

                    $this->setRelations($parent, $node);
                }
            }

            if ($node && !$node->getParentId()) {
                $items[] = $node;
            }
        }

        dd($items);

        return new static($items);
    }

    protected function setRelations($parent, $child)
    {
        $children = $parent->getRelation('children');

        if (!$children) {
            $children = BaseCollection::make([$child]);
        } else {
            $children->push($child);
        }

        $parent->setRelation('children', $children);
        $child->setRelation('parent', $parent);
    }

}