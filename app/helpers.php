<?php 

if(!function_exists('content_locale_url')){
    function content_locale_url($path, ?string $locale = null){
        return url(($locale ?: App::getContentLocale()).'/'.trim($path, '/'));
    }
}