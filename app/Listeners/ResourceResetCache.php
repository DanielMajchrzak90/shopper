<?php

namespace App\Listeners;

use App\Events\ResourceChanged;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\MenuItem;
use App\Models\Category;

class ResourceResetCache
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ResourceChanged  $event
     * @return void
     */
    public function handle(ResourceChanged $event)
    {
        MenuItem::shopCacheTreeByLocales();
        Category::shopCacheTreeByLocales();
    }
}
