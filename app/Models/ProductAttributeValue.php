<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Builder;
use DB;

class ProductAttributeValue extends Pivot
{
    use HasFactory; 
    use Traits\Translatable;

    protected $table = 'product_attribute_values';

    public $translatable = ['text_value'];

    protected $translationClass = ProductAttributeValueTranslation::class;
    protected $translationKey = 'pa_id'; 

    protected $fillable = [
        'bool_value',
        'option_value',
        'num_value',
        'attribute_id',
        'product_id',
    ];

    protected $with = [
        'attribute', 
        'option'
    ];

    public $incrementing = true;
    public $timestamps = false;

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }

    public function option()
    {
        return $this->belongsTo(AttributeOption::class, 'option_value');
    }
}
