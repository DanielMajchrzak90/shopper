<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class MenuItemTranslation extends Model
{
    use HasFactory, Cachable;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'slug',
        'image_title',
    ];
}
