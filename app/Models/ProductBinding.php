<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductBinding extends Model
{
    use HasFactory, Traits\TranslateWith;

    protected $table = 'products_bindings';

    public function products()
    {
        return $this->hasMany(Product::class, 'products_binding_id');
    }

    public function unbindProducts()
    {
        $this->products->each(function($product) {
            $product->update([
                'products_binding_id' => null,
            ]);
        });
    }

    public function bindProducts(array $relatedProducts)
    {
        $products = Product::find($relatedProducts);
        $products->each(function($product) {
            $product->update([
                'products_binding_id' => $this->id,
            ]);
        });
    }
}
