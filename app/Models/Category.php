<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Cache;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use App;
use ACME\Nestedset\Collection;
use Arr;
use DB;
use App\Http\Controllers\Shop\CategoriesController;
use App\Http\Controllers\Shop\ProductsController;

class Category extends Model
{
    use HasFactory, Traits\Translatable, NodeTrait;

    public $translatable = ['name', 'slug'];

    protected $fillable = ['parent_id', 'group_id'];

    protected $appends = [
        'url',
    ];

    protected $translationClass = CategoryTranslation::class;
    protected $translationKey = 'category_id';

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }   

    public function group()
    {
        return $this->belongsTo(AttributeGroup::class, 'group_id');
    }   

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function menuItems()
    {
        return $this->hasMany(MenuItem::class, 'category_id');
    }
    
    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function attributes()
    {
        return Attribute::whereHas('groups', function($query) {
            $query->where('attribute_groups.id', $this->group_id);
        })->get();
    }

    public function variantAttributes()
    {
        return $this->attributes()->filter(fn($attribute) => $attribute->is_variant);
    }

    public function scopeDepthFilter($query, $depth)
    {
        $sub = DB::table('categories as parent_cat')
            ->selectRaw('count(*)')
            ->whereColumn('parent_cat._lft', '<', 'categories._lft')
            ->whereColumn('parent_cat._rgt', '>', 'categories._rgt');

        return $query->where(DB::raw('('.$sub->toRawSql().')'), $depth);
    }

    public function getUrlAttribute()
    {
        if (!$slug = $this->slug) {
            return null;
        }

        return content_locale_url($slug, $this->locale);
    }    

    public function controller()
    {
        return $this->children->count() > 0 ? 
            CategoriesController::class: 
            ProductsController::class; 
    }

    public function action()
    {
        return 'index';
    }

    public function productsView()
    {
        $this->translateLoad(['ancestors']);

        $breadcrumb = $this->ancestors
            ->map(fn($ancestor) => ([
                'text' => $ancestor->name,
                'href' => $ancestor->url,
            ]));
        $breadcrumb->push([
            'text' => $this->name,
            'href' => $this->url,
        ]);
        $breadcrumb->prepend([
            'text' => 'Start',
            'href' => url('/'),
        ]);

        $products = $this->products()->translate()
            ->translateWith([
                'image',
                'binding.products.image',
                'attributesValues.option',
                'attributesValues.attribute',
                'variants.attributesValues.attribute',
                'variants.attributesValues.option',
                'binding.products.attributesValues.attribute',
                'binding.products.variants.attributesValues.attribute',
                'binding.products.variants.attributesValues.option',
                'binding.products.attributesValues.option',
            ])
            ->groupBy(DB::raw('ifnull(products_binding_id,id)'))
            ->paginate(100);

        $attributes = $this->group->attributes()
            ->translate()
            ->translateWith([
                'options'
            ])
            ->get();

        return view('shop.products', [
            'products' => $products,
            'attributes' => $attributes,
            'breadcrumb' => $breadcrumb,
            'category' => $this,
        ]);
    }

    public function categoriesView()
    {
        return view('shop.categories', [
            'model' => $this
        ]);        
    }

    public static function shopCacheTreeByLocales()
    {
        $locales = Locale::get();

        foreach ($locales as $locale) {
            self::cacheShopTreeByLocale($locale->code, true);
        }
    } 

    public static function cacheShopTreeKey(string $locale)
    {
        return 'shop_tree_'.$locale;
    }

    public static function forgetCacheShopTreeByLocale(string $locale)
    {
        Cache::forget(self::cacheShopTreeKey($locale));
    }

    public static function cacheShopTreeByLocale(?string $locale = null, $force = false)
    {
        $locale = $locale ?: App::getContentLocale();

        if ($force) {
            self::forgetCacheShopTreeByLocale($locale);
        }

        return Cache::rememberForever(self::cacheShopTreeKey($locale), function() use ($locale) {
            return self::shopTreeByLocale($locale);
        });
    }

    public static function shopTreeByLocale(?string $locale = null)
    {
        $categories = self::defaultOrder()
            ->translate($locale)
            ->get()
            ->toTree();


        $map = function ($category) use (&$map) {
            $data = [
                'id' => $category->id,
                'name' => $category->name,
                'locale' => $category->locale,
            ];

            if ($category->children->count() > 0) {
                $data['children'] = $category->children->map($map)->toArray();
            } else {
                $data['url'] = $category->url;
            }

            return $data;
        };

        return $categories->map(function ($category) use ($map) {
            return $map($category);
        })->toArray();
    } 
}
