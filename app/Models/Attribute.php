<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ACME\DTOS\AttributeOptionDTO;
use App\Enums\AttributeType;
use Illuminate\Support\Collection;
use ACME\DTOS\Base\ProductAttributeValueDTO;

class Attribute extends Model
{
    use HasFactory, Traits\Translatable;

    public $translatable = ['label'];

    protected $translationClass = AttributeTranslation::class;
    protected $translationKey = 'attribute_id'; 

    protected $fillable = [
        'type', 'name', 'is_variant'
    ];

    protected $with = [
        // 'options'
    ];

    protected $appends = [
        'component'
    ];

    protected $casts = [
        'is_variant' => 'boolean'
    ];

    public function groups()
    {
        return $this->belongsToMany(
            AttributeGroup::class, 
            'attribute_group_pivot',
            'attribute_id',
            'group_id'
        );
    }

    public function options()
    {
        return $this->hasMany(AttributeOption::class, 'attribute_id')->orderBy('id', 'desc');
    }

    public function saveOptions(array $options)
    {
        $options = collect($options);
        $ids = $options->map(fn($col) => $col->getId())->filter();

        $this->options()->whereNotIn('id', $ids->toArray())->delete();
        
        $options = $options->reverse();

        foreach ($options as $option) {  
            if ($option->isNew()) {
                $this->options()->create($option->toArray());
            } else {
                $modelOption = $this->options()->findOrFail($option->getId());
                $modelOption->update($option->toArray());
            }
        }
    }

    public function getClassTypeAttribute()
    {
        $type = $this->type;

        return class_exists($type) ? (new $type)->setAttribute($this) : null;
    }

    public function getEnumTypeAttribute()
    {
        return new AttributeType($this->type);
    }

    public function getComponentAttribute()
    {
        return optional($this->class_type)->getComponent();
    }

    public function shopRender()
    {
        return view('shop.attributes.'.$this->class_type->getShopComponent(), [
            'attribute' => $this
        ])->render();
    }
}
