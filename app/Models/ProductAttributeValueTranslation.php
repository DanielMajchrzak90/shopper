<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttributeValueTranslation extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'product_attribute_value_translations';

    protected $fillable = [
        'text_value',
    ];
}
