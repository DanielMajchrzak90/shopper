<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class MenuItemCol extends Model
{
    use HasFactory, Traits\Translatable, Cachable;

    protected $fillable = [
        'item_id',
        'position'
    ];

    public $translatable = [
        'name',
    ];

    protected $translationClass = MenuItemColTranslation::class;
    protected $translationKey = 'col_id'; 

    public function items()
    {
        return $this->belongsToMany(
            MenuItem::class, 
            'menu_item_col_pivot',
            'col_id',
            'item_id'
        );
    }

    public function parent()
    {
        return $this->belongsTo(MenuItem::class, 'item_id');
    }

    public function getRidAttribute()
    {
        return 'rid'.uniqid();
    }
}
