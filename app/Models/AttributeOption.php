<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use ACME\Casts\AdditionalColumnAttribute;

class AttributeOption extends Model
{
    use HasFactory, Traits\Translatable;
    
    public $translatable = ['name'];

    protected $translationClass = AttributeOptionTranslation::class;
    protected $translationKey = 'attribute_option_id'; 

    protected $fillable = [
        'additional', 
    ];

    protected $casts = [
        'additional' => AdditionalColumnAttribute::class
    ];

    public function getRidAttribute()
    {
        return 'rid'.uniqid();
    }
}
