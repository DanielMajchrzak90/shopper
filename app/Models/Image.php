<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'filename'
    ];

    protected $appends = [
        'image_url',
        'thumbnail_url',
    ];

    public function getImageUrlAttribute()
    {
        return asset('storage/images/'.$this->filename);
    }

    public function getThumbnailUrlAttribute()
    {
        return asset('storage/thumbnails/'.$this->filename);
    }
}
