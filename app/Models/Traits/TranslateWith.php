<?php 

namespace App\Models\Traits;

use App;
use App\Models\Locale;
use Cache;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;

trait TranslateWith
{
    public function scopeTranslateWith($query, $with, ?string $locale = null)
    {
        return $this->handleTranslateWith($query, $this->mapUndotWith($with), $locale);
    }

    public function handleTranslateWith($query, array $with, ?string $locale = null)
    {
        return $query->with($this->handleTranslate($with, $locale));
    }

    public function translateLoad(array $with, ?string $locale = null)
    {
        return $this->handleTranslateLoad($this->mapUndotWith($with), $locale);
    }
    
    protected function mapUndotWith(array $with)
    {
        $withArray = [];
        
        foreach ($with as $key => $value) {
            if ($value instanceof Closure) {
                Arr::set($withArray, $key, $value);
            } else {
                Arr::set($withArray, $value, null);
            }
        }

        return $withArray;
    }

    protected function handleTranslate(array $with, ?string $locale = null)
    {
        $mappedWith = [];

        foreach ($with as $key => $value) {
            $mappedWith[$key] = function($query) use ($value, $locale) {
                try {
                    $query->translate($locale);
                } catch (\Throwable $e) {}

                if (is_array($value)) {
                    $this->handleTranslateWith($query, $value, $locale);
                } elseif ($value instanceof Closure) {
                    $value($query);
                }
            };
        }

        return $mappedWith;
    }

    public function handleTranslateLoad(array $with, ?string $locale = null)
    {
        return $this->load($this->handleTranslate($with, $locale));
    }

}