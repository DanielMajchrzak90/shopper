<?php 

namespace App\Models\Traits;

use App;
use App\Models\Locale;
use Cache;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Arr;
use Illuminate\Database\Eloquent\Model;
use App\Models\TranslateBulider;

trait Translatable
{
    use TranslateWith;

    protected static $fallbackLocale = true;
    protected static $locale = null;
    protected static $locales = null;
    protected $localesAttributes = [];

    public function translations()
    {
        return $this->hasMany($this->translationClass, $this->translationKey);
    }

    public function translation()
    {
        return $this->hasOne($this->translationClass, $this->translationKey);
    }

    public function scopeFallbackTranslate($query, bool $fallbackLocale = true)
    {
        $this->fallbackLocale = $fallbackLocale;

        return $query;
    }

    public function scopeTranslate($query, ?string $locale = null, $fallbackLocale = true)
    {
        $translationClass = $this->translationClass;
        $translationKey = $this->translationKey;
        $translationsTable = (new $translationClass)->getTable();
        $table = $this->getTable();

        $translatable = $this->translatable;
        $translatable[] = 'locale';

        foreach ($translatable as $index => $value) {
            $translatable[$index] = $translationsTable.'.'.$value;
        }

        $sub = DB::table($table)->select("{$table}.*")
            ->addSelect($translatable) 
            ->leftJoin($translationsTable, function($query) use (
                $translationClass, $translationsTable, $translationKey, $table, $locale, $fallbackLocale
            ) {
                $locale = $locale ?: App::getContentLocale();
                $loceleSub = $translationClass::selectRaw('count(*)')
                    ->whereColumn("$table.id", "$translationsTable.$translationKey")
                    ->where('locale', $locale);

                $query->on("$table.id", '=', "$translationsTable.$translationKey")
                    ->when($fallbackLocale, function($query) use ($loceleSub, $locale, $translationsTable) {
                        $query->on("$translationsTable.locale", '=', DB::raw("if(({$loceleSub->toRawSql()}) > 0,'$locale',$translationsTable.locale)"));
                    }, function ($query) use ($locale, $translationsTable)  {
                        $query->on("$translationsTable.locale", '=', DB::raw("'$locale'"));
                    });
            });

        return $query->from(DB::raw("({$sub->toRawSql()}) as $table"));
    }

    public function saveTranslation(array $data, string $locale)
    {
        $locale = $locale ?: App::getContentLocale();

        $this->translations()->updateOrCreate([
            'locale' => $locale
        ], $data);
    }



    public function saveTranslations()
    {
        foreach ($this->localesAttributes as $locale => $data) {
            $this->saveTranslation($data, $locale);
        }
    }

    public static function boot(): void
    {
        parent::boot();
        static::saved(function (Model $model) {
            $model->saveTranslations();
        });
    }

    // protected static function booted()
    // {
    //     static::addGlobalScope('translate', function (Builder $builder) {
    //         $builder->translate(self::$locale, self::$fallbackLocale);
    //     });
    // }

    public static function setLocale(?string $locale = null)
    {
        self::$locale = $locale;
    }

    public static function setFallbackLocale(bool $fallbackLocale = true)
    {
        self::$fallbackLocale = $fallbackLocale;
    }

    protected static function locales()
    {
        if (!self::$locales) {
            return self::$locales = Locale::get()->pluck('code');
        }

        return self::$locales;
    }

    public function fill(array $attributes)
    {                
        foreach ($attributes as $key => $values) {
            if (
                self::locales()->contains($key)
                && is_array($values)
            ) {
                $this->localesAttributes[$key] = $values;
                unset($attributes[$key]);
            } 
        }

        return parent::fill($attributes);
    }
}