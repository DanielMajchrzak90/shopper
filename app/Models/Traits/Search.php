<?php 

namespace App\Models\Traits;

use App;
use App\Models\Locale;
use Cache;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;

trait Search
{
    public function scopeSearch(
        $query,
        $phrase
    )
    {
        return $query->Where(DB::raw("CONCAT_WS(' ',".implode(',', $this->searchColumns).")"),  'like', "%$phrase%");
    }
}