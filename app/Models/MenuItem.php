<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Arr;
use ACME\DTOS\MenuItemColDTO;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Storage;
use Cache;
use App;

class MenuItem extends Model
{
    use HasFactory, Traits\Translatable;

    protected $fillable = [
        'category_id',
        'query',
        'col_id',
        'image',
        'image_link'
    ];

    public $translatable = [
        'name',
        'slug',
        'image_title',
    ];

    protected $translationClass = MenuItemTranslation::class;
    protected $translationKey = 'item_id';  

    protected $appends = [
        'name_or_category_name',
        'image_url',
        'image_full_link',
    ];

    protected $with = [
        'category', 'cols',
    ];

    const SELECT_CACHE_KEY = 'menu_items_select';
    const TREE_JSON_CACHE_KEY = 'menu_items_tree_json';

    public function cols()
    {
        return $this->hasMany(MenuItemCol::class, 'item_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function saveCols(array $cols) : self
    {
        $cols = collect($cols);
        $colsIds = $cols->map(fn($col) => $col->getId())->filter();

        $colsToDelete = $this->cols()->whereNotIn('id', $colsIds)->delete();

        foreach ($cols as $col) {            
            if (!$col->isNew()) {
                $modelCol = $this->cols()->find($col->getId());
                $modelCol->update($col->toArray());
            } else {
                $modelCol = $this->cols()->create($col->toArray());
            }

            $modelCol->items()->detach();

            if ($items = $col->getItems()) {
                $modelCol->items()->attach($items);
            }
        }

        return $this;
    }

    public function scopeParentFilter($query)
    {
        return $query->whereHas('cols');
    }

    public function getNameOrCategoryNameAttribute()
    {
        return $this->name ?: optional($this->category)->name;
    }

    public function getImageUrlAttribute()
    {
        if (!$this->attributes['image']) {
            return null;
        }

        return asset('storage/images/'.$this->attributes['image']);
    }

    public function getImageFullLinkAttribute()
    {
        if (!$this->attributes['image_link']) {
            return null;
        }

        return url($this->attributes['image_link']);
    }

    public function saveImage(?File $image, ?string $name) : self
    {
        if (!$image) {
            return $this;
        }
        
        $this->destroyImage();

        $imagesDisk = Storage::disk('images');
        
        if (!$name) {
            $name = $image->getFilename();
        }

        $name = time().'_'.$name;
        $imagesDisk->put($name, $image->getContent());

        $this->update([
            'image' => $name,
        ]);

        return $this;
    }

    public function destroyImage() : self
    {
        $imagesDisk = Storage::disk('images');
        
        if ($imagesDisk->exists($this->image)) {
            $imagesDisk->delete($this->image);
        }

        $this->update([
            'image' => null
        ]);

        return $this;
    }

    public function getUrlAttribute()
    {
        $category = $this->category;

        if (!$this->slug && !$category) {
            return null;
        }

        return $this->slug ? content_locale_url($this->slug) : $category->url;
    }

    public static function shopCacheTreeByLocales()
    {
        $locales = Locale::get();

        foreach ($locales as $locale) {
            self::cacheShopTreeByLocale($locale->code, true);
        }
    } 

    public static function cacheShopTreeKey(string $locale)
    {
        return 'shop_menu_item_tree_'.$locale;
    }

    public static function forgetCacheShopTreeByLocale(string $locale)
    {
        Cache::forget(self::cacheShopTreeKey($locale));
    }

    public static function cacheShopTreeByLocale(?string $locale = null, $force = false)
    {
        $locale = $locale ?: App::getContentLocale();

        if ($force) {
            self::forgetCacheShopTreeByLocale($locale);
        }

        return Cache::rememberForever(self::cacheShopTreeKey($locale), function() use ($locale) {
            return self::shopTreeByLocale($locale);
        });
    }

    public static function shopTreeByLocale(?string $locale = null)
    {
        $menuItems = self::parentFilter()
            ->translate($locale)
            ->translateWith([
                'category',
                'cols.items.category'
            ])
            ->get();

        $tree = [];

        foreach ($menuItems as $menuItem) {
            $cols = [];
            foreach ($menuItem->cols as $col) {
                $colItems = [];
                foreach ($col->items as $colItem) {
                    $colItems[] = [
                        'name' => $colItem->name_or_category_name,
                        'url' => $colItem->url,
                        'locale' => $colItem->locale,
                    ];
                }
                $cols[] = [
                    'name' => $col->name,
                    'items' => $colItems,
                ];
            }
            $tree[] = [
                'name' => $menuItem->name_or_category_name,
                'image_url' => $menuItem->image_url,
                'image_full_link' => $menuItem->image_full_link,
                'cols' => $cols
            ];
        }

        return $tree;
    } 
}
