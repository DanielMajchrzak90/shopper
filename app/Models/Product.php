<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Arr;
use Illuminate\Support\Collection;
use Storage;
use Image as InerventionImage;
use Symfony\Component\HttpFoundation\File\File;
use DB;
use App\Http\Controllers\Shop\ProductsController;

class Product extends Model
{
    use HasFactory, Traits\Translatable, Traits\Search;

    public $translatable = ['name', 'slug', 'description'];

    protected $translationClass = ProductTranslation::class;
    protected $translationKey = 'product_id';  

    protected $fillable = [
        'category_id',
        'sku',
        'price',
        'quantity',
        'is_active',
        'product_id',
        'products_binding_id',
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $appends = [
        'rid'
    ];

    protected $searchColumns = [
        'slug', 'name', 'description',
    ];

    public function attributes()
    {
        return $this->belongsToMany(
            Attribute::class, 
            'product_attribute_values',
            'product_id',
            'attribute_id'
        )->using(ProductAttributeValue::class);
    }

    public function binding()
    {
        return $this->belongsTo(ProductBinding::class, 'products_binding_id');
    }

    public function attributesValues() 
    {
        return $this->hasMany(ProductAttributeValue::class, 'product_id');
    }

    public function image() 
    {
        return $this->hasOne(Image::class, 'product_id');
    }

    public function images() 
    {
        return $this->hasMany(Image::class, 'product_id');
    }

    public function variants() 
    {
        return $this->hasMany(Product::class, 'product_id')->latest();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function saveAttributes(array $attributeDtos)
    {
        $attributeDtos = collect($attributeDtos);
        $ids = $attributeDtos->map(function($attributeDto) {
            return $attributeDto->getAttributeId();
        })->filter();

        $this->attributesValues()->whereNotIn('attribute_id', $ids->toArray())->delete();

        $attributeDtos->each(function($attributeDto) {
            $this->attributesValues()->updateOrCreate([
                'attribute_id' => $attributeDto->getAttributeId(),
            ], $attributeDto->toArray());
        });
    }

    public function saveVariants(array $variantDtos)
    {
        $variantDtos = collect($variantDtos);
        $ids = $variantDtos->map(function($dto) {
            return $dto->getId();
        })->filter();

        $variantsToRemove = $this->variants()->whereNotIn('id', $ids->toArray())->get();
        $variantsToRemove->each(function($variant) {
            $variant->delete();
        });

        $variantDtos->each(function($dto) {
            if ($dto->isNew()) {
                $product = $this->variants()->create($dto->toArray());
            } else {
                $product = $this->variants()->find($dto->getId());
                $product->update($dto->toArray());
            }

            $product->saveAttributes($dto->getAttributes());
        });
    }

    public function saveImages(array $images)
    {
        $imageModels = [];
        $imagesDisk = Storage::disk('images');
        $thumbnailsDisk = Storage::disk('thumbnails');

        foreach ($images as $image) {

            if(!$image instanceof File) {
              throw new \Exception("image must be instance of ".File::class);              
            }

            $name = $image->getFilename();
            $name = time().'_'.$name;

            $imagesDisk->put($name, $image->getContent());

            $thumbnail = InerventionImage::make($image->getRealPath());
            $thumbnail->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->encode($image->getExtension());

            $thumbnailsDisk->put($name, $thumbnail);
            $imageModels[] = new Image([
                'filename' => $name
            ]);
        }
        
        $this->images()->saveMany($imageModels);
    }

    public function deleteImages(array $ids)
    {
        $images = $this->images()->find($ids);
        $images->each(function($image) {
            $image->delete();
        });
    }

    public function getRidAttribute()
    {
        return 'rid'.uniqid();
    }

    public function scopeNoVariantsFilter($query)
    {
        return $query->whereNull('products.product_id');
    }

    public function getRelatedProductsAttribute() : array
    {
        $products = optional($this->binding)->products; 

        if (!$products) {
            return [];
        }

        return $products->pluck('id')->toArray();
    }

    public function saveRelatedProducts(array $relatedProducts)
    {
        if (count($relatedProducts) == 0) {
            $this->update([
                'products_binding_id' => null,
            ]);
        } else {
            $relatedProducts[] = $this->id;

            $binding = ProductBinding::create();
            $binding->bindProducts($relatedProducts);
        }

        $bindindsToDestroy = ProductBinding::doesntHave('products')->delete();
    }

    public function view()
    {
        return view('shop.product', [
            'model' => $this
        ]);
    }
    
    public function controller()
    {
        return ProductsController::class; 
    }

    public function action()
    {
        return 'show';
    }

    public function getPriceFormatAttribute()
    {
        return number_format(
            $this->price,
            2,
            ',',
            ' '
        ).' PLN' ;
    }

    public function getAdditionalAttributesDescAttribute() : array
    {
        $customAttributes = [];

        foreach ($this->attributesValues as $value) {
            $attribute = $value->attribute;
            $customAttributes[$attribute->name] = $attribute->class_type->getDescValue($value);
        }

        return $customAttributes;
    }

    public function getAdditionalAttributesAttribute() : array
    {
        $customAttributes = [];

        foreach ($this->attributesValues as $value) {
            $attribute = $value->attribute;
            $customAttributes[$attribute->name] = $attribute->class_type->getValue($value);
        }

        return $customAttributes;
    }

    public function getCustomAttributesAttribute()
    {
        $attributes = [];
        
        foreach ($this->attributesValues as $value) {
            $attribute = $value->attribute;
            $attributes[$attribute->name] = $attribute->class_type->getDescValue($value);
        }

        return $attributes;
    }

    public function getCustomAttribute($attribute)
    {
        return Arr::get($this->custom_attributes, $attribute);
    }

    public function getAttributeVariants($attribute)
    {
        $variants = $this->variants;

        $attributeVariants = $variants->map(fn($variant) => $variant->getCustomAttribute($attribute));
        $attributeVariants->push($this->getCustomAttribute($attribute));

        return $attributeVariants
            ->filter()
            ->unique()
            ->values();
    }

    public function getUrlAttribute()
    {
        if (!$slug = $this->slug) {
            return null;
        }

        return content_locale_url($slug, $this->locale);
    }  

    public function getViewAttribute()
    {
        return 'product';
    }
}
