<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    protected $with = [
        'attributes'
    ];

    public function attributes()
    {
        return $this->belongsToMany(
            Attribute::class, 
            'attribute_group_pivot',
            'group_id',
            'attribute_id'
        );
    }
}
