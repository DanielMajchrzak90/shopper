<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attribute;
use App\Http\Controllers\Ajax\AjaxController;
use App\Http\Requests\Ajax\Attributes\StoreRequest;
use App\Http\Requests\Ajax\Attributes\UpdateRequest;
use DB;
use ACME\DTOS\Base\AttributeDTO;
use ACME\DTOS\Base\AttributeTranslationDTO;
use ACME\DTOS\Base\AttributeOptionDTO;
use ACME\DTOS\Base\AttributeOptionTranslationDTO;
use ACME\Transformers\AttributeTransformer;
use Arr;

class AttributesController extends AjaxController
{
    public function __construct()
    {
        $this->middleware('trueFallbackLocale')->only('index');
    }

    public function index(Request $request)
    {
        $attributes = Attribute::translate()->paginate();
        $attributes->getCollection()
            ->transform(function($attribute) use ($request) {     
                return $attribute->only(['id', 'name', 'label', 'type', 'enum_type']);
            });

        return $this->response([
            'attributes' => $attributes
        ]);
    }

    public function store(StoreRequest $request)
    {
        $attribute = DB::transaction(function() use ($request) {
            $type = resolve($request->type);
            $dto = $this->resolveDTOFromRequest($request);    
            $type->save($dto);

            return $attribute;
        });

        return $this->response([
            'message' => __('messages.created', ['item' => __('messages.attribute')]),
            'attribute' => $attribute
        ]);
    }

    public function update(UpdateRequest $request)
    {
        DB::transaction(function() use ($request) {
            $attribute = $request->route('attribute');
            $type = resolve($request->type);
            $dto = $this->resolveDTOFromRequest($request);    
            $type->setAttribute($attribute)->save($dto);
        });

        return $this->response([
            'message' => __('messages.updated', ['item' => __('messages.attribute')])
        ]);
    }

    public function show(Request $request)
    {
        $attribute = $request->route('attribute');
        $attribute->translateLoad(['options']);

        return $this->response([
            'attribute' => (new AttributeTransformer($attribute))->toArray()
        ]);
    }

    public function destroy(Request $request)
    {
        $attribute = $request->route('attribute');
        $attribute->delete();

        return response()->json([
            'message' => __('messages.deleted', ['item' => __('messages.attribute')])
        ]);
    }

    protected function resolveDTOFromRequest(Request $request)
    {
        $locale = \App::getContentLocale();
        $dto = new AttributeDTO();

        $dto
            ->setType($request->type)
            ->setName($request->name)
            ->setIsVariant($request->is_variant)
            ->addTranslation(
                (new AttributeTranslationDTO)
                    ->setLocale($locale)
                    ->setLabel($request->label)
            );

        foreach ($request->getArray('options') as $option) {
            $dto->addOption(
                (new AttributeOptionDTO)
                    ->setAdditional(Arr::get($option, 'additional'))
                    ->setId(Arr::get($option, 'id'))
                    ->addTranslation(
                        (new AttributeOptionTranslationDTO)
                            ->setLocale($locale)
                            ->setName(Arr::get($option, 'name'))
                    )
            );
        }

        return $dto;
    }
}
