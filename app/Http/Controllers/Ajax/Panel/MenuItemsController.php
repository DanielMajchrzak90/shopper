<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\AjaxController;
use App\Models\MenuItem;
use App\Http\Requests\Ajax\MenuItems\StoreRequest;
use App\Http\Requests\Ajax\MenuItems\UpdateRequest;
use ACME\DTOS\Base\MenuItemDTO;
use ACME\DTOS\Base\MenuItemTranslationDTO;
use ACME\DTOS\Base\MenuItemColDTO;
use ACME\DTOS\Base\MenuItemColTranslationDTO;
use ACME\Transformers\MenuItemTransformer;
use DB;
use Arr;

class MenuItemsController extends AjaxController
{
    public function __construct()
    {
        $this->middleware('trueFallbackLocale')->only('index');
    }

    public function index(Request $request)
    {
        $menuItems = MenuItem::translate()
            ->translateWith(['category'])
            ->paginate();

        return $this->response([
            'menu_items' => $menuItems
        ]);
    }

    public function store(StoreRequest $request)
    {
        $menuItem = DB::transaction(function() use ($request) {
            $dto = $this->resolveDTOFromRequest($request);         
            $menuItem = MenuItem::create($dto->toArray());
            $menuItem->saveCols($dto->getCols());

            return $menuItem;
        });

        return response()->json([
            'message' => __('messages.created', ['item' => __('messages.menu_item')]),
            'menu_item' => $menuItem
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $menuItem = DB::transaction(function() use ($request) {
            $dto = $this->resolveDTOFromRequest($request);   
            $menuItem = $request->route('menu_item');  

            if ($request->destroy_image) {
                $menuItem->destroyImage();
            }
            
            $menuItem->update($dto->toArray());
            $menuItem->saveCols($dto->getCols())
                ->saveImage($dto->getImage(), optional($dto->getImage())->getClientOriginalName());

            return $menuItem;
        });

        return response()->json([
            'message' => __('messages.updated', ['item' => __('messages.menu_item')])
        ]);
    }

    public function show(Request $request)
    {
        $menuItem = $request->route('menu_item');
        $menuItem->load(['cols' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }]);

        return $this->response([
            'menu_item' => (new MenuItemTransformer($menuItem))
                    ->toArray()
        ]);
    }

    public function destroy(Request $request)
    {
        $menuItem = $request->route('menu_item');
        $menuItem->delete();

        return response()->json([
            'message' => __('messages.deleted', ['item' => __('messages.menu_item')])
        ]);
    }

    protected function resolveDTOFromRequest(Request $request)
    {
        $locale = \App::getContentLocale();
        $dto = new MenuItemDTO();

        $dto->setCategoryId($request->category_id)
            ->setImageLink($request->image_link)
            ->setImage($request->image)
            ->addTranslation(
                (new MenuItemTranslationDTO)
                    ->setLocale($locale)
                    ->setName($request->name)
                    ->setSlug($request->slug)
                    ->setImageTitle($request->image_title)
            );

        foreach ($request->getArray('cols') as $col) {
            $colDto = (new MenuItemColDTO)
                ->setId(Arr::get($col, 'id'))
                ->addTranslation(
                    (new MenuItemColTranslationDTO)
                        ->setLocale($locale)
                        ->setName(Arr::get($col, 'name'))
                );

            foreach (Arr::get($col, 'items', []) as $id) {
                $colDto->addItem($id);
            }

            $dto->addCol($colDto);
        }

        return $dto;
    }
}
