<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\AjaxController;
use App\Models\Category;
use App\Http\Requests\Ajax\Categories\StoreRequest;
use App\Http\Requests\Ajax\Categories\UpdateRequest;
use ACME\DTOS\Base\CategoryDTO;
use ACME\DTOS\Base\CategoryTranslationDTO;
use ACME\Transformers\CategoryTransformer;

class CategoriesController extends AjaxController
{
    public function __construct()
    {
        $this->middleware('trueFallbackLocale')->only('index');
    }

    public function index(Request $request)
    {
        $categories = Category::translateWith(['parent'])
            ->translate()
            ->paginate();

        $categories->getCollection()
            ->transform(function($attribute) use ($request) {     
                return $attribute->only(['id', 'name', 'slug', 'parent_id', 'parent']);
            });

        return $this->response([
            'categories' => $categories
        ]);
    }

    public function store(StoreRequest $request)
    {
        $dto = $this->resolveDTOFromRequest($request);         
        $category = Category::create($dto->toArray());

        return response()->json([
            'message' => __('messages.created', ['item' => __('messages.category')]),
            'category' => $category
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $dto = $this->resolveDTOFromRequest($request);  
        $category = $request->route('category');
        $category->update($dto->toArray());

        return response()->json([
            'message' => __('messages.updated', ['item' => __('messages.category')])
        ]);
    }

    public function show(Request $request)
    {
        $category = $request->route('category');

        return $this->response([
            'category' => (new CategoryTransformer($category))
                    ->toArray()
        ]);
    }

    public function destroy(Request $request)
    {
        $category = $request->route('category');
        $category->delete();

        return response()->json([
            'message' => __('messages.deleted', ['item' => __('messages.category')])
        ]);
    }

    protected function resolveDTOFromRequest(Request $request)
    {
        $locale = \App::getContentLocale();
        $dto = new CategoryDTO();

        $dto
            ->setParentId($request->parent_id)
            ->setGroupId($request->group_id)
            ->addTranslation(
                (new CategoryTranslationDTO)
                    ->setLocale($locale)
                    ->setName($request->name)
                    ->setSlug($request->slug)
            );

        return $dto;
    }
}
