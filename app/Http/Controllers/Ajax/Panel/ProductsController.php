<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\AjaxController;
use App\Models\Product;
use App\Http\Requests\Ajax\Products\StoreRequest;
use App\Http\Requests\Ajax\Products\UpdateRequest;
use ACME\DTOS\Base\ProductDTO;
use ACME\DTOS\Base\ProductAttributeValueDTO;
use ACME\DTOS\Base\ProductAttributeValueTranslationDTO;
use ACME\DTOS\Base\ProductTranslationDTO;
use ACME\DTOS\Base\ProductVariantDTO;
use ACME\DTOS\Base\ProductImageDTO;
use ACME\Transformers\ProductTransformer;
use DB;
use Storage;
use Image;

class ProductsController extends AjaxController
{
    public function __construct()
    {
        $this->middleware('trueFallbackLocale')->only('index');
    }

    public function index(Request $request)
    {
        $products = Product::noVariantsFilter()
            ->translate()
            ->when($request->phrase, function($query) use ($request) {
                $query->search($request->phrase);
            })
            ->with('image')
            ->paginate();

        return $this->response([
            'products' => $products
        ]);
    }

    public function store(StoreRequest $request)
    {
        $product = DB::transaction(function() use ($request) {
            $dto = $this->resolveDTOFromRequest($request);
            $product = Product::create($dto->toArray());
            $product->saveAttributes($dto->getAttributes());
            $product->saveImages($dto->getImages());
            $product->saveVariants($dto->getVariants());
            $product->saveRelatedProducts($dto->getRelatedProducts());

            return $product;
        });

        return $this->response([
            'message' => __('messages.created', ['item' => __('messages.product')]),
            'product' => $product
        ]);
    }

    public function update(UpdateRequest $request)
    {
        DB::transaction(function() use ($request) {
            $dto = $this->resolveDTOFromRequest($request);
            $product = $request->route('product');
            $product->update($dto->toArray());
            $product->saveAttributes($dto->getAttributes());
            $product->saveImages($dto->getImages());
            $product->deleteImages($dto->getDeletedImages());
            $product->saveVariants($dto->getVariants());
            $product->saveRelatedProducts($dto->getRelatedProducts());
        });

        return response()->json([
            'message' => __('messages.updated', ['item' => __('messages.product')])
        ]);
    }

    public function show(Request $request)
    {
        $product = $request->route('product');
        $product->translateLoad([
            'attributesValues',
            'variants.attributesValues'
        ]);
        $product->load('images');
        $product->append('related_products');

        return $this->response([
            'product' => (new ProductTransformer($product))
                    ->toArray()
        ]);
    }

    public function destroy(Request $request)
    {
        $product = $request->route('product');
        $product->delete();

        return response()->json([
            'message' => __('messages.deleted', ['item' => __('messages.product')])
        ]);
    }

    protected function resolveDTOFromRequest(Request $request)
    {
        $locale = \App::getContentLocale();
        
        $dto = new ProductDTO();
        $dto
            ->setSku($request->sku)
            ->setIsActive($request->is_active)
            ->setPrice($request->price)
            ->setQuantity($request->quantity)
            ->setCategoryId($request->category_id)
            ->addTranslation(
                (new ProductTranslationDTO())
                    ->setLocale($locale)
                    ->setName($request->name)
                    ->setSlug($request->slug)
                    ->setDescription($request->description)
            );

        foreach ($request->getArray('related_products') as $id) {
            $dto->addRelatedProduct($id);
        }

        foreach ($request->getArray('deleted_images')  as $id) {
            $dto->addDeletedImage($id);
        }

        $category = $request->getCategory();

        foreach ($request->getArray('variants')  as $key => $variant) {
            $variantKey = 'variants.'.$key.'.';

            $variantDTO = (new ProductVariantDTO)
                ->setId($request->input($variantKey.'id'))
                ->setPrice($request->input($variantKey.'price'))
                ->setQuantity($request->input($variantKey.'quantity'));

            foreach ($category->variantAttributes() as $attribute) {
                $variantDTO->addAttribute(
                    $attribute->class_type->setValueDTO(
                        (new ProductAttributeValueDTO())->setAttributeId($attribute->id), 
                        $request->input($variantKey.$attribute->name),
                        $locale
                    )
                );
            }

            $dto->addVariant($variantDTO);
        }

        foreach ($category->attributes() as $attribute) {
            $dto->addAttribute(
                $attribute->class_type->setValueDTO(
                    (new ProductAttributeValueDTO())->setAttributeId($attribute->id), 
                    $request->input($attribute->name),
                    $locale
                )
            );
        }

        foreach ($request->getArray('uploaded_images') as $image) {
            $dto->addImage($image);
        }

        return $dto;
    }
}
