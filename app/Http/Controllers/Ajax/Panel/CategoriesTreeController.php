<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Ajax\CategoriesTree\UpdateRequest;
use App\Models\Category;

class CategoriesTreeController extends Controller
{
    public function index(Request $request)
    {
        return response()->json([
            'categories' => Category::defaultOrder()->translate()->get()->toTree()
        ]);
    }

    public function update(UpdateRequest $request)
    {
        Category::rebuildTree($request->tree);

        return response()->json([
            'message' => __('messages.updated', [
                'item' => __('messages.category')
            ]),
        ]);
    }

}
