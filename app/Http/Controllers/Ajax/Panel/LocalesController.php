<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Ajax\Locales\StoreRequest;
use App\Http\Requests\Ajax\Locales\UpdateRequest;
use App\Models\Locale;

class LocalesController extends Controller
{
    public function index()
    {
        return response()->json([
            'locales' => Locale::get()
        ]);
    }

    public function store(StoreRequest $request)
    {
        $locale = Locale::create($request->validated());

        return response()->json([
            'message' => __('messages.created', ['item' => __('messages.locale')])
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $locale = $request->route('locale');
        $locale->update($request->validated());

        return response()->json([
            'message' => __('messages.updated', ['item' => __('messages.locale')])
        ]);
    }

    public function show(Request $request)
    {       
        return response()->json([
            'locale' => Locale::findOrFail($request->route('locale_id'))
        ]); 
    }

    public function destroy(Request $request)
    {
        $locale = $request->route('locale');
        $locale->delete();

        return response()->json([
            'message' => __('messages.deleted', ['item' => __('messages.locale')])
        ]);
    }
}
