<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Enums\AttributeType;

class EnumsView extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json([
            'attributes' => AttributeType::asSelectArray()
        ]);
    }
}
