<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\AjaxController;
use App\Models\AttributeGroup;
use App\Http\Requests\Ajax\AttributeGroups\StoreRequest;
use DB;
use ACME\Transformers\AttributeGroupTransformer;

class AttributeGroupsController extends AjaxController
{
    public function index(Request $request)
    {
        $groups = AttributeGroup::paginate();
        $groups->getCollection()
            ->transform(function($group) { 
                return (new AttributeGroupTransformer($group))->toArray();
            });

        return $this->response([
            'groups' => $groups
        ]);
    }

    public function store(StoreRequest $request)
    {
        $group = DB::transaction(function() use ($request) {
            $group = AttributeGroup::create($request->only(['name']));
            $group->attributes()->sync($request->getArray('attributes'));

            return $group;
        });

        return $this->response([
            'message' => __('messages.created', ['item' => __('messages.attribute_group')]),
            'group' => $group
        ]);
    }

    public function update(StoreRequest $request)
    {
        $group = DB::transaction(function() use ($request) {
            $group = $request->route('attribute_group');
            $group->update($request->only(['name']));
            $group->attributes()->sync($request->getArray('attributes'));

            return $group;
        });

        return $this->response([
            'message' => __('messages.updated', ['item' => __('messages.attribute_group')]),
            'group' => $group
        ]);
    }

    public function show(Request $request)
    {
        $group = $request->route('attribute_group');

        return $this->response([
            'group' => (new AttributeGroupTransformer($group))->toArray()
        ]);
    }

    public function destroy(Request $request)
    {
        $group = $request->route('attribute_group');
        $group->delete();

        return response()->json([
            'message' => __('messages.deleted', ['item' => __('messages.attribute_group')])
        ]);
    }
}
