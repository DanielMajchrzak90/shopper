<?php

namespace App\Http\Controllers\Ajax\Panel\Selects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MenuItem;
use App\Http\Controllers\Ajax\AjaxController;
use Cache;

class MenuItemsView extends AjaxController
{
    public function __invoke(Request $request)
    {
        $items = Cache::rememberForever(MenuItem::SELECT_CACHE_KEY, function () {
            $items = MenuItem::get();
            $items->transform(function($item) {     
                    return [
                        'id' => $item->id,
                        'label' => $item->name_or_category_name,
                    ];
                });

            return $items;
        });

        return $this->response([
            'menu_items' => $items
        ]);
    }
}
