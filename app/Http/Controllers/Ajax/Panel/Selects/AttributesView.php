<?php

namespace App\Http\Controllers\Ajax\Panel\Selects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attribute;
use App\Http\Controllers\Ajax\AjaxController;

class AttributesView extends AjaxController
{
    public function __invoke(Request $request)
    {
        $attributes = Attribute::get();
        $attributes
            ->transformLocale($request->getMyLocale())
            ->transform(function($attribute) use ($request) {     
                return [
                    'id' => $attribute->id,
                    'label' => $attribute->label,
                ];
            });

        return $this->response([
            'attributes' => $attributes
        ]);
    }
}
