<?php

namespace App\Http\Controllers\Ajax\Panel\Selects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Controllers\Ajax\AjaxController;
use Cache;
use App;
class CategoryView extends AjaxController
{
    public function __invoke(Request $request)
    {
        $category = $request->route('category');

        $categories = Category::defaultOrder()
            ->translate()
            ->when($category, function($query) use ($category) {
                $query->whereNotDescendantOf($category)
                    ->whereNotDescendantOf($category)
                    ->where('id', '<>', $category->id);
            })
            ->get()
            ->toTree();

        $map = function ($category) use (&$map) {
            $data = [
                'id' => $category->id,
                'label' => $category->name,
            ];

            if ($category->children->count() > 0) {
                $data['children'] = $category->children->map($map);
            }

            return $data;
        };

        $categories = $categories->map(function ($category) use ($map) {
            return $map($category);
        });
                
        return $this->response([
            'categories' => $categories
        ]);
    }
}
