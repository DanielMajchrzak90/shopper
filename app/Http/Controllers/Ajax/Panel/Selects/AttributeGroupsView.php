<?php

namespace App\Http\Controllers\Ajax\Panel\Selects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AttributeGroup;
use App\Http\Controllers\Ajax\AjaxController;

class AttributeGroupsView extends AjaxController
{
    public function __invoke(Request $request)
    {
        $groups = AttributeGroup::get();
        $groups
            ->transform(function($group) use ($request) {     
                return [
                    'id' => $group->id,
                    'label' => $group->name,
                ];
            });

        return $this->response([
            'groups' => $groups
        ]);
    }
}
