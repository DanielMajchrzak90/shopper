<?php

namespace App\Http\Controllers\Ajax\Panel\Selects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\AjaxController;
use App\Models\Product;
use DB;
class ProductsView extends AjaxController
{
    public function __invoke(Request $request)
    {
        $products = Product::translate()
            ->noVariantsFilter()
            ->get();

        $products->transform(function($product) {     
            return [
                'id' => $product->id,
                'label' => $product->name,
            ];
        });

        return $this->response([
            'products' => $products
        ]);
    }
}
