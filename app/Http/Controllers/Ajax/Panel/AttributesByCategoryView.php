<?php

namespace App\Http\Controllers\Ajax\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\AjaxController;
use App;

class AttributesByCategoryView extends AjaxController
{
    public function __construct()
    {
        $this->middleware('trueFallbackLocale');
    }
    
    public function __invoke(Request $request)
    {
        $category = $request->route('category');

        return $this->response([
            'attributes' => $category
                ->group
                ->attributes()
                ->translateWith(['options'])
                ->translate()
                ->get()
        ]);
    }
}
