<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\ControllerDispatcher;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $model = $request->route('slug');

        if ($model) {
            $route = app(\Illuminate\Routing\Route::class);
            $container = app();

            return (new ControllerDispatcher($container))
                ->dispatch($route, resolve($model->controller()), $model->action());
        }

        return view('admin.dashboard');
    }
}
