<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $model = $request->route('slug');
        
        return view('shop.categories', [
            'model' => $model
        ]); 
    }
}
