<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProductsController extends Controller
{
    public function index(Request $request)
    {       
        // dd($request->all());
        // $priceFrom = $request->input('price.from');
        // $priceTo = $request->input('price.to');
        // dd($priceFrom, $priceTo, $request->all());

        $category = $request->route('slug');
        $model->translateLoad(['ancestors']);

        $breadcrumb = $category->ancestors
            ->map(fn($ancestor) => ([
                'text' => $ancestor->name,
                'href' => $ancestor->url,
            ]));
        $breadcrumb->push([
            'text' => $category->name,
            'href' => $category->url,
        ]);
        $breadcrumb->prepend([
            'text' => 'Start',
            'href' => url('/'),
        ]);

        $products = $category->products()->translate()
            ->translateWith([
                'image',
                'binding.products.image',
                'attributesValues.option',
                'attributesValues.attribute',
                'variants.attributesValues.attribute',
                'variants.attributesValues.option',
                'binding.products.attributesValues.attribute',
                'binding.products.variants.attributesValues.attribute',
                'binding.products.variants.attributesValues.option',
                'binding.products.attributesValues.option',
            ])
            ->groupBy(DB::raw('ifnull(products_binding_id,id)'))
            ->paginate(100);

        $attributes = $category->group->attributes()
            ->translate()
            ->translateWith([
                'options'
            ])
            ->get();

        return view('shop.products', [
            'products' => $products,
            'attributes' => $attributes,
            'breadcrumb' => $breadcrumb,
            'category' => $category,
        ]);
    }

    public function show(Request $request)
    {
        $category = $request->route('slug');

        return view('shop.product', [
            'model' => $category
        ]);
    }
}
