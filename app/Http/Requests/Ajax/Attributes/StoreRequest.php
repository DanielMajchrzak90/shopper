<?php

namespace App\Http\Requests\Ajax\Attributes;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Enums\AttributeType;
use App\Rules\SnakeCase;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required',
            'name' => [
                'required', 
                new SnakeCase,
                Rule::unique('attribute_translations')
            ],
            'type' => [    
                'required',
                Rule::in(AttributeType::getValues()),
            ],
            'is_variant' => 'boolean',
            'options' => [
                'array'
            ],
            'options.*.name' => [
                'required',
            ],
            'options.*.additional.color' => [
                Rule::requiredIf($this->type == AttributeType::COLOR_TYPE),
            ],
        ];
    }
}
