<?php

namespace App\Http\Requests\Ajax\Attributes;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\Slug;
use App\Rules\SnakeCase;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $attribute = $this->route('attribute');

        $rules = parent::rules();
        $rules['name'] = [
            'required', 
            new SnakeCase,
            Rule::unique('attributes')
                ->ignore(
                    $attribute->id, 
                    'id'
                )
        ];

        return $rules;
    }
}
