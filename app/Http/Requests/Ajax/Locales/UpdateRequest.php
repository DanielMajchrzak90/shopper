<?php

namespace App\Http\Requests\Ajax\Locales;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Slug;
use Illuminate\Validation\Rule;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $locale = $this->route('locale');
        $rules = parent::rules();
        $rules['code'] = [
            'required', 
            new Slug, 
            Rule::unique('locales')->ignore($locale->id)
        ];

        return $rules;
    }
}
