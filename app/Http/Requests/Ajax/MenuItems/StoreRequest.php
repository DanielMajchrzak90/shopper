<?php

namespace App\Http\Requests\Ajax\MenuItems;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\Slug;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                Rule::requiredIf(!$this->category_id),
                'string',
                'max:100',
                'nullable',
            ],
            'slug' => [
                Rule::requiredIf(!$this->category_id),
                new Slug, 
                Rule::unique('menu_item_translations'),
                'nullable',
            ],
            'category_id' => [
                Rule::requiredIf(!$this->name && !$this->slug),
                Rule::exists('categories', 'id'), 
                'nullable'
            ],
            'cols' => 'array',
            'cols.*.name' => 'required|string|max:100',
            'cols.*.items' => 'array',
            'image_title' => 'required_with:image,image_link',
            'image_link' => 'required_with:image_title,image',
            'image' => 'required_with:image_title,image_link|image',
        ];
    }
}
