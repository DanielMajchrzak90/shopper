<?php

namespace App\Http\Requests\Ajax\MenuItems;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\Slug;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $menuItem = $this->route('menu_item');
        $rules = parent::rules();
        $rules['slug'] = [
            Rule::requiredIf(!$this->category_id),
            new Slug, 
            Rule::unique('menu_item_translations')
                ->ignore(optional($menuItem->translate($this->getMyLocale()))->id),
            'nullable',
        ];
        $rules['destroy_image'] = 'boolean|nullable';
        $rules['image'] = [
            'image',
            Rule::requiredIf(($this->image_title || $this->image_link) && !$menuItem->image)
        ];

        return $rules;
    }
}
