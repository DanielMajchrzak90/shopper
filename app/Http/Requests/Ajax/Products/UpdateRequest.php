<?php

namespace App\Http\Requests\Ajax\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\Slug;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = $this->route('product');

        $rules = parent::rules();
        $rules['sku'] = [
            'required',
            'string',
            'max:100',
            Rule::unique('products')
                ->ignore($product->id),
            new Slug, 
        ];
        $rules['slug'] = [
            'required', 
            new Slug, 
            Rule::unique('product_translations')
                ->where(
                    fn ($query) => $query->where('product_id', '<>', $product->id)
                        ->where('locale', '<>', $product->locale)
                )
        ];
        $rules['deleted_images'] = 'array';
        $rules['deleted_images.*'] = [
            Rule::exists('images', 'id')->where(function ($query) use ($product) {
                return $query->where('product_id', $product->id);
            })
        ];

        return $rules;
    }
}
