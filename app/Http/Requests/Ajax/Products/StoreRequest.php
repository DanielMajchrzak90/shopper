<?php

namespace App\Http\Requests\Ajax\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\Slug;
use App\Models\Category;

class StoreRequest extends FormRequest
{
    protected $category;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'sku' => [
                'required',
                'string',
                'max:100',
                Rule::unique('products'),
                new Slug
            ],
            'is_active' => 'boolean',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'name' => 'required|string|max:100',
            'description' => 'required|string',
            'slug' => ['required', new Slug, Rule::unique('product_translations')],
            'category_id' => [Rule::exists('categories', 'id'), 'required'],
            'uploaded_images' => 'array',
            'uploaded_images.*' => 'image',
            'related_products' => 'array',
            'related_products.*' => [Rule::exists('products', 'id'), 'required'],
        ];

        if (!$attributes = $this->getAttributes()) {
            return $rules;
        }

        foreach ($attributes as $attribute) {
            if (!$attribute->class_type) {
                continue;
            }

            $rules[$attribute->name] = $attribute->class_type
                ->setAttribute($attribute)
                ->rules();
        }

        $rules['variants'] = 'array';
        $rules['variants.*.id'] = [Rule::exists('products', 'id'), 'nullable'];
        $rules['variants.*.price'] = 'required|numeric';
        $rules['variants.*.quantity'] = 'required|numeric';

        if (!$variantAttributes = $this->getVariantAttributes()) {
            return $rules;
        }

        foreach ($variantAttributes as $attribute) {
            if (!$attribute->class_type) {
                continue;
            }

            $rules['variants.*.'.$attribute->name] = $attribute->class_type
                ->setAttribute($attribute)
                ->rules();
        }

        return $rules;
    }

    public function getCategory()
    {
        if ($this->category) {
            return $this->category;
        }

        return $this->category = Category::find($this->category_id);
    }

    public function getAttributes()
    {
        return optional($this->getCategory())->attributes();
    }

    public function getVariantAttributes()
    {
        return optional($this->getCategory())->variantAttributes();
    }

    public function attributes()
    {
        $attributeFields = [];

        if (!$attributes = $this->getAttributes()) {
            return $attributeFields;
        }

        foreach ($attributes as $attribute) {
            $attributeFields[$attribute->name] = strtolower($attribute->label);
        }

        if (!$attributes = $this->getVariantAttributes()) {
            return $attributeFields;
        }

        foreach ($attributes as $attribute) {
            $attributeFields['variants.*.'.$attribute->name] = strtolower($attribute->label);
        }

        return $attributeFields;
    }
}
