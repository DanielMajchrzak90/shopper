<?php

namespace App\Http\Requests\Ajax\Categories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\Slug;
use App\Models\Category;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $category = $this->route('category');
        $rules = parent::rules();
        $rules['slug'] = [
            'required', 
            new Slug, 
            Rule::unique('category_translations')
                ->ignore(optional($category->translate($this->getMyLocale()))->id)
        ];
        $rules['parent_id'] = [
            Rule::exists('categories', 'id'),
            function ($attribute, $value, $fail) use ($category) {
                $categories = Category::descendantsAndSelf($category->id);
                
                if (in_array($value, $categories->pluck('id')->toArray())) {
                    $fail(__('validation.category_parent_not_same', [
                        'attribute' => $attribute
                    ]));
                }
            },
            'nullable',
        ];

        return $rules;
    }
}
