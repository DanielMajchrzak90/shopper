<?php

namespace App\Http\Requests\Ajax\Categories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\Slug;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'slug' => ['required', new Slug, Rule::unique('category_translations')],
            'parent_id' => [Rule::exists('categories', 'id')],
            'group_id' => [Rule::exists('attribute_groups', 'id'), 'nullable'],
        ];
    }
}
