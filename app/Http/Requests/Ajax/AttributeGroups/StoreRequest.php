<?php

namespace App\Http\Requests\Ajax\AttributeGroups;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'attributes' => 'required|array|min:1',
            'attributes.*' => 'exists:attributes,id'
        ];
    }
}
