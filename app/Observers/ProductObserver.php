<?php

namespace App\Observers;

use App\Models\Product;
use Cache;

class ProductObserver
{
    /**
     * Handle the MenuItem "created" event.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return void
     */
    public function created(Product $product)
    {
        
    }

    /**
     * Handle the MenuItem "updated" event.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return void
     */
    public function updated(Product $product)
    {

    }

    /**
     * Handle the MenuItem "deleted" event.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return void
     */
    public function deleted(Product $product)
    {

    }

    /**
     * Handle the MenuItem "restored" event.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the MenuItem "force deleted" event.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
