<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use App\Models\Locale;
use App;
use App\Models\Category;
use App\Models\Product;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('slug', function ($slug) {
            $model = null;
            $resources = [Category::class, Product::class];

            foreach ($resources as $resource) {
                $model = $resource::translate()
                    ->where('slug', $slug)
                    ->first();

                if ($model) {
                    break;
                }
            }
            
            return $model;
        });
        Route::bind('attribute', function ($id) {
            return \App\Models\Attribute::translate()->findOrFail($id);
        });
        Route::bind('attribute_group', function ($id) {
            return \App\Models\AttributeGroup::findOrFail($id);
        });
        Route::bind('locale', function ($id) {
            return \App\Models\Locale::findOrFail($id);
        });
        Route::bind('category', function ($id) {
            return \App\Models\Category::translate()->findOrFail($id);
        });
        Route::bind('menu_item', function ($id) {
            return \App\Models\MenuItem::translate()->findOrFail($id);
        });
        Route::bind('product', function ($id) {
            return \App\Models\Product::translate()->findOrFail($id);
        });
        
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php')); 

            Route::group([
                'prefix' =>  \LaravelLocalization::setLocale(),
                'middleware' => [ 
                    'localizationRedirect', 
                    'localeCookieRedirect', 
                    'contentLocale'
                ],
            ], function($route) {     
                \App::setLocale(\LaravelLocalization::getCurrentLocale());
                
                $routes = function() {                    
                    Route::prefix('ajax')
                        ->middleware('ajax')
                        ->group(base_path('routes/ajax.php'));

                    Route::middleware('web')
                        ->namespace($this->namespace)
                        ->group(base_path('routes/web.php'));
                };

                $locales = 'en';
                
                try {
                    $locales = Locale::get()->pluck('code')->implode('|');
                } catch (\Throwable $e) {}

                Route::group([
                        'prefix' => '{content_locale}', 
                        'where' => ['content_locale' => $locales]
                    ], function() use ($routes) {
                        $routes();
                    });

                $routes();
            });
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
