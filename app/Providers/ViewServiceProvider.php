<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Models\MenuItem;
use App\Models\Category;
use Cache;
use App;
use Carbon\Carbon;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {        
        View::composer('shop.master', function ($view) {
            $view
                ->with('categoriesTree', Category::cacheShopTreeByLocale())
                ->with('menuItemsTree', MenuItem::cacheShopTreeByLocale());
        });
    }
}
