<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\Models\Locale;
use Illuminate\Database\Eloquent\Collection;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Request::macro(
            'localesRules', 
            function ($callback) {
                return Locale::get()->pluck('code')->map(function($code) use ($callback) {
                    return collect($callback())->mapWithKeys(function($val, $attr) use ($code) {
                        return [
                            $code.'.'.$attr => $val,
                        ];
                    });
                })->toArray();
            }
        );

        Request::macro(
            'localeRules', 
            function ($callback) {
                return collect($callback())->mapWithKeys(function($val, $attr) {
                    $locale = $this->getLocale();
                    
                    return [
                        $locale.'.'.$attr => $val,
                    ];
                })->toArray(); 
            }
        );

        Request::macro(
            'transOnly', 
            function ($fields, $transFields) {
                $locale = $this->getMyLocale();

                return array_merge(
                    $this->only($fields),
                    [
                        $locale => $this->only($transFields)
                    ]
                ); 
            }
        );

        Request::macro(
            'getMyLocale', 
            function () {
                return $this->route('content_locale') ?: \App::getLocale(); 
            }
        );

        Request::macro(
            'getArray', 
            function ($input) {
                $input = $this->$input;
                return is_array($input) ? $input : []; 
            }
        );

        $toRawSql = function() {

            $sql = $this->toSql();

            foreach($this->getBindings() as $binding) {
                $value = is_numeric($binding) ? $binding : "'".$binding."'";
                $sql = preg_replace('/\?/', $value, $sql, 1);
            }

            return $sql;
        };

        \Illuminate\Database\Query\Builder::macro(
            'toRawSql', $toRawSql
        );

        \Illuminate\Database\Eloquent\Builder::macro(
            'toRawSql', $toRawSql
        );
    }
}
