@extends('shop.master')

@section('category')
@json($category->toArray())
@endsection

@section('breadcrumb')
<b-breadcrumb :items='@json($breadcrumb->toArray())'></b-breadcrumb>
@endsection

@section('content')
<h1 class="mb-3">{{$category->name}}</h1>
@php
DB::connection()->enableQueryLog();
@endphp
<attributes-form
	url="{{request()->url()}}"
	:query='@json(request()->input('phrase'))'>
	<template #default="{model, clearFiltersKey}">
		<b-row>
			<b-col class="col">
				<component 
					is="number-field"
					name="price"
					:label="lang.get('messages.price')"
					:query='@json(request()->input('price'))'
					:model="model"
					ref="price"></component>
			</b-col>
			<b-col class="col" lg="2">
				<component 
					is="radio-field"
					name="order"
					:label="lang.get('messages.order')"
					:options="['cheapest', 'most_expensive', 'newest'].map((item) => ({
						name: lang.get('messages.'+item),
						value: item
					}))"
					:query='@json(request()->input('order'))'
					:model="model"
					ref="order"></component>
			</b-col>
			@foreach ($attributes as $index => $attribute)
				@if ($attribute->class_type->getShopComponent())
					<b-col class="col" lg="2">
						<component 
							is="{{$attribute->class_type->getShopComponent()}}"
							:dropright='@json((($index + 2) % 5) == 0 ? true : false)'
							name="{{$attribute->name}}"
							label="{{$attribute->label}}"
							:options='@json($attribute->options)'
							:query='@json(request()->input($attribute->name))'
							:model="model"
							ref="{{$attribute->name}}"></component>
					</b-col>
				@endif
			@endforeach
		</b-row>		
	</template>
</attributes-form>
<div class="grid-p-sm">
	<b-row>
		@foreach ($products as $product)
			<b-col lg="4" class="col">
				<product-card
					:img-src='@json(optional($product->image)->thumbnail_url)'
		    		title="{{$product->name}}"
		    		sub-title="{{$product->getAttributeVariants('size')->implode(' | ')}}"
		    		price="{{ $product->price_format}}"
		    		:url='@json($product->url)'
		    		key="{{$product->id}}">
		    		@if($product->binding)
		    		<template #thumbs>  
			    		<b-row>  			
				    		@foreach ($product->binding->products->take(4) as $bindProduct)
				    			<binding-product-thumb 
									:src='@json(optional($bindProduct->image)->thumbnail_url)'
									:url='@json($bindProduct->url)'
									title="{{$bindProduct->name}}"
									sub-title="{{$bindProduct->getAttributeVariants('size')->implode(' | ')}}"
		    						key="{{$bindProduct->id}}"
		    						price="{{ $bindProduct->price_format}}"
									>
								</binding-product-thumb>
				    		@endforeach
				    	</b-row>
		    		</template>
		    		@endif
		    	</product-card>
		    </b-col>
		@endforeach   
	</b-row>
</div>
@php
Log::info(DB::getQueryLog());
@endphp
<div class="d-flex justify-content-center">  
	{{ $products->links('pagination::bootstrap-4') }}
</div>
@endsection