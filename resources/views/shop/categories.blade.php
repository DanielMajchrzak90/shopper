@extends('shop.master')

@section('category')
@json($model->load('ancestors')->toArray())
@endsection

@section('breadcrumb')
@php
$items = $model->ancestors->map(fn($ancestor) => ([
	'text' => $ancestor->name,
	'href' => $ancestor->url,
]));
$items->push([
	'text' => $model->name,
	'href' => $model->url,
]);
$items->prepend([
	'text' => 'Start',
	'href' => url('/'),
]);
@endphp
<b-breadcrumb :items='@json($items->toArray())'></b-breadcrumb>
@endsection

@section('content')
<h1>{{$model->name}}</h1>
<b-card-group columns>
	@foreach ($model->children as $child)
		<b-card
    		title="{{$child->name}}">			
		</b-card>
	@endforeach
</b-card-group>
@endsection