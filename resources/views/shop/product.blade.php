@extends('shop.master')

@section('breadcrumb')
@php
$category = $model->category()->translate()->first();
$items = $category->ancestors()
	->translate()
	->get()
	->map(fn($ancestor) => ([
		'text' => $ancestor->name,
		'href' => $ancestor->url,
	]));
$items->push([
	'text' => $category->name,
	'href' => $category->url,
]);
$items->push([
	'text' => $model->name,
	'href' => $model->url,
]);
$items->prepend([
	'text' => 'Start',
	'href' => url('/'),
]);
@endphp
<b-breadcrumb :items='@json($items->toArray())'></b-breadcrumb>
@endsection

@section('sidebar', '')

@section('content')
<b-row>
	<b-col md="3">
		<b-img thumbnail src="{{$model->image->image_url}}" alt="Image 1"></b-img></b-col>
	<b-col>
		<h1 class="mb-3">{{$model->name}}</h1>
	</b-col>
</b-row>
@php
DB::connection()->enableQueryLog();

// dd($products->getCollection()->get(3)->toArray());
@endphp

@php
Log::info(DB::getQueryLog());
@endphp
@endsection