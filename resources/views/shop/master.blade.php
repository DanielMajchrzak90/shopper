<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('app.name')}}</title>
    <meta 
      name="description" 
      content="Panel admina">
    <link rel="canonical" href="{{url('/')}}">
    <link 
      rel="stylesheet" 
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
    <link href="{{ mix('css/shop/app.css')}}" rel="stylesheet" type="text/css"> 
  </head>
  <body>
    <div 
      id="startLoader">
      <div class="spinner-grow" role="status" style="width: 5rem; height: 5rem;">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <div id="app">
      <app>
        <layout>
          <template #menu-items>
            <menu-items 
              :menu-items='@json($menuItemsTree)'>
            </menu-items>
          </template>
          @section('sidebar')
            <template #sidebar>
              <sidebar
                :categories='@json($categoriesTree)'
                :current-category='@yield('category')'>
              </sidebar>
            </template>
          @show
          @yield('breadcrumb')
          @yield('content')
        </layout>
      </app>
    </div>
    <input type="hidden" id="appName" value="{{config('app.name')}}"/>
    <script src="{{ mix('js/shop/app.js') }}"></script>   
  </body>
</html>