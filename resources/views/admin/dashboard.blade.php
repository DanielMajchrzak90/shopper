<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('app.name')}}</title>
    <meta 
      name="description" 
      content="Panel admina">
    <link rel="canonical" href="{{url('/')}}">
    <link 
      rel="stylesheet" 
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
    <link href="{{ mix('css/admin/app.css')}}" rel="stylesheet" type="text/css"> 
  </head>
  <body>
    <div id="app">
      <app></app>
    </div>
    <input type="hidden" id="appName" value="{{config('app.name')}}"/>
    <input type="hidden" id="attributeTypes" value='@json(App\Enums\AttributeType::getAttributeTypesArray())'/>
    <script src="{{ mix('js/admin/app.js') }}"></script>   
  </body>
</html>