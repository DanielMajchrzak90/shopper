@extends('shop.master')

@section('category')
@json(null)
@endsection

@section('breadcrumb')
@php
$items = collect([]);
$items->prepend([
	'text' => 'Start',
	'href' => url('/'),
]);
@endphp
<b-breadcrumb :items='@json($items->toArray())'></b-breadcrumb>
@endsection

@section('content')
<h1>Start</h1>
@endsection