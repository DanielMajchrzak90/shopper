<?php 

use App\Enums\AttributeType;

return [
	AttributeType::class => [
		AttributeType::TEXT_TYPE => 'Text',
		AttributeType::BOOL_TYPE => 'Switcher',
		AttributeType::SELECT_TYPE => 'Select',
		AttributeType::COLOR_TYPE => 'Color',
		AttributeType::NUMBER_TYPE => 'Number',
	],
];