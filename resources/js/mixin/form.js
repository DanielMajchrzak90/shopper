import PendingMixin from './pending'

export default {
  mixins: [PendingMixin],
	data() {
		return {
			form: {},
			errors: {},
		}
	},
	computed: {
	},
	provide() {
    return {
    	getError: this.getError,
	    getErrorState: this.getErrorState
	  }
  },
	methods: {
		async handleErrors(method, callback = null) {
			this.errors = {}
			try {
				return await method()
			} catch (e) {
				if (e.response) {
					this.$bvToast.toast(e.response.data.message, {
	          title: 'Wystąpił błąd.',
	          autoHideDelay: 2000,
	          variant: 'danger'
	        })

	        if (e.response.status == 422) {
	        	this.errors = e.response.data.errors
	        }
	        if (e.response.status == 401) {
	        	this.$router.push({
	        		name: 'login'
	        	})
	        }
				} 

				if (callback) {
					callback()
				}
				
				throw e
			}
		},
		async handlePending(method, type = 'pending') {
			this[type] = true
			try {
				let response = await method()
				this[type] = false

				return response
			} catch (e) {
				this[type]  = false
				throw e
			}
		},
		getError(name) {
			let error = this.errors[name];

  		return error ? error[0] : null;
		},
		getErrorState(name) {
			let error = this.errors[name];

  		return error ? false : null;
		},
		getLocaleError(name) {
			return this.getError(this.locale+'.'+name)
		},
		getLocaleErrorState(name) {
			return this.getErrorState(this.locale+'.'+name)
		},
		resetErrors() {
			this.errors = {}
		}
	}
}