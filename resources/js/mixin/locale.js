export default {	
  computed: {
  	lang() {
  		return this.$store.state.lang
  	},
    locale() {
      return this.$store.getters['getLocale']
    }
  },
}