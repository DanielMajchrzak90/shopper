export default {	
  methods: {
    routeLocale(route) {
      return {
        ...route,
        query: route.query,
        params: {
          ...route.params,
          content_locale: this.$route.params.content_locale          
        }
      }
    }
  },
}