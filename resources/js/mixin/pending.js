export default {
	data() {
		return {
			pending: false,
		}
	},
	methods: {
		async handlePending(method, type = 'pending') {
			this[type] = true
			try {
				let response = await method()
				this[type] = false

				return response
			} catch (e) {
				this[type]  = false
				throw e
			}
		},
	}
}