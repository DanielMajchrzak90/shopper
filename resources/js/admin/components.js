import Vue from 'vue'

import AuthLayout from '@/layouts/AuthLayout'
import PanelLayout from '@/layouts/PanelLayout'
import GuestLayout from '@/layouts/GuestLayout'
import StartLayout from '@/layouts/StartLayout'
import ShopLayout from '@/layouts/ShopLayout'
import PendingButton from '@/admin/components/PendingButton'
import DestroyButton from '@/admin/components/DestroyButton'
import Loader from '@/admin/components/Loader'
import PaginationWrapper from '@/admin/components/PaginationWrapper'
import PageFooter from '@/components/Footer'
import BackButton from '@/admin/components/BackButton'
import LocalesDropdown from '@/admin/components/LocalesDropdown'
import PendingWrapper from '@/admin/components/PendingWrapper'
import Accordion from '@/admin/components/Accordion'
import AccordionCollapse from '@/admin/components/AccordionCollapse'
import Treeselect from '@riophae/vue-treeselect'
import TopProgressbar from '@/admin/components/TopProgressbar'
import ImagePreview from '@/admin/components/ImagePreview'


Vue.component('auth-layout', AuthLayout)
Vue.component('panel-layout', PanelLayout)
Vue.component('guest-layout', GuestLayout)
Vue.component('start-layout', StartLayout)
Vue.component('shop-layout', ShopLayout)
Vue.component('pending-button', PendingButton)
Vue.component('destroy-button', DestroyButton)
Vue.component('loader', Loader)
Vue.component('pagination-wrapper', PaginationWrapper)
Vue.component('page-footer', PageFooter)
Vue.component('back-button', BackButton)
Vue.component('locales-dropdown', LocalesDropdown)
Vue.component('pending-wrapper', PendingWrapper)
Vue.component('pending-wrapper', PendingWrapper)
Vue.component('accordion', Accordion)
Vue.component('accordion-collapse', AccordionCollapse)
Vue.component('treeselect', Treeselect)
Vue.component('top-progressbar', TopProgressbar)
Vue.component('image-preview', ImagePreview)