import baseFetch from './base-fetch'

const fetch = baseFetch({
  allAttribute: 'categories',
  oneAttribute: 'category',
})

const state = {
  ...fetch.state,
  categoriesSelect: [],
}

const getters = {
  ...fetch.getters,
  categoriesSelect(state) {
    return state.categoriesSelect
  }
}

const actions = {  
  fetchAll: (store, query) => fetch.actions.fetchAll(store, (api) => api.categories.fetchAll(query)),  
  fetchOne: (store, {id, query}) => fetch.actions.fetchOne(store, (api) => api.categories.edit(id, query)),
  fetchCategoriesSelect: async ({rootGetters, commit}, {id, query}) => {
    let api = rootGetters['panelApi']
    let {categories} = await api.selects.categories(query, id)
    commit('setCategoriesSelect', categories)
  }
}

const mutations = {
  ...fetch.mutations,
  setCategoriesSelect(state, categories) {
    state.categoriesSelect = categories
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}