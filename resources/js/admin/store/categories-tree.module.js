import baseFetch from './base-fetch'

const fetch = baseFetch({
  allAttribute: 'categories',
})

const state = {
  ...fetch.state,
}

const getters = {
  ...fetch.getters,
}

const actions = {  
  fetch: (store, query) => fetch.actions.fetchAll(store, (api) => api.categoriesTree.fetch(query)),  
}

const mutations = {
  ...fetch.mutations,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}