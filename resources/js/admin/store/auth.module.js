import ApiService from "@/common/api.service";
import JwtService from "@/common/jwt.service";

const state = {
  user: null,
  token: JwtService.getToken()
}

const getters = {
  user(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return !!state.user
  },
  token(state) {
    return state.token
  }
}

const actions = {  
  async login({commit, rootState, getters, dispatch, rootGetters}, payload) {
    var data = await rootGetters.api.auth.login(payload)

    commit('setToken', data.access_token)

    return data
  },
  async register({commit, rootState, getters, dispatch, rootGetters}, payload) {
    var data = await rootGetters.api.auth.register(payload)

    return data
  },
  async forgotPassword({commit, rootState, getters, dispatch, rootGetters}, payload) {
    var data = await rootGetters.api.auth.forgotPassword(payload)

    return data
  },
  async resetPassword({commit, rootState, getters, dispatch, rootGetters}, payload) {
    var data = await rootGetters.api.auth.resetPassword(payload)

    return data
  },
  async checkAuth({ commit, getters, rootState, rootGetters }) {
    try {
      var data = await rootGetters.api.auth.checkAuth()
      
      commit('setUser', data)

    } catch (error) {
      commit('purgeAuth')
    }
  },
  async logout({getters, commit, rootGetters}) {
    var data = await rootGetters.api.auth.logout()
   
    commit('purgeAuth')

    return data
  },
}

const mutations = {
  setUser(state, user) {
    state.user = user;
  },
  purgeAuth(state) {
    state.user = null
    state.token = null
    JwtService.destroyToken()
  },
  setToken(state, token) {
    JwtService.saveToken(token);
    state.token = token
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}