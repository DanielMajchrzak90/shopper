import baseFetch from './base-fetch'

const state = {
  started: false,
  finished: false,
}

const getters = {
  started(state) {
    return state.started
  },
  finished(state) {
    return state.finished
  }
}

const actions = {  
  
}

const mutations = {
  start(state) {
    state.started = true
  },
  finish(state) {
    state.finished = true
  },
  reset(state) {
    state.finished = false
    state.started = false
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}