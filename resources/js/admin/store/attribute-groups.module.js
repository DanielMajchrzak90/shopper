import baseFetch from './base-fetch'

const fetch = baseFetch({
  allAttribute: 'groups',
  oneAttribute: 'group',
})

const state = {
  ...fetch.state,
  attributeGroupsSelect: [],
}

const getters = {
  ...fetch.getters,
  attributeGroupsSelect(state) {
    return state.attributeGroupsSelect
  }
}

const actions = {  
  fetchAll: (store, query) => fetch.actions.fetchAll(store, (api) => api.attributeGroups.fetchAll(query)),
  fetchOne: (store, {id, query}) => fetch.actions.fetchOne(store, (api) => api.attributeGroups.edit(id, query)), 
  fetchAttributeGroupsSelect: async ({rootGetters, commit}, {query}) => {
    let api = rootGetters['panelApi']
    let {groups} = await api.selects.attributeGroups(query)
    commit('setAttributeGroupsSelect', groups)
  }
}

const mutations = {
  ...fetch.mutations,
  setAttributeGroupsSelect(state, groups) {
    state.attributeGroupsSelect = groups
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}