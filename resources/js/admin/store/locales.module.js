import baseFetch from './base-fetch'

const fetch = baseFetch({
  allAttribute: 'locales',
  oneAttribute: 'locale',
})

const state = {
  ...fetch.state,
  locales: [],
}

const getters = {
  ...fetch.getters
}

const actions = {  
  fetchAll: (store, query) => fetch.actions.fetchAll(store, (api) => api.locales.fetchAll(query)),
}

const mutations = {
  ...fetch.mutations
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}