import baseFetch from './base-fetch'

const fetch = baseFetch({
  allAttribute: 'attributes',
  oneAttribute: 'attribute',
})

const state = {
  ...fetch.state,
  attributesSelect: [],
}

const getters = {
  ...fetch.getters,
  attributesSelect(state) {
    return state.attributesSelect
  },
  attributesTypes() {
    let types = $('#attributeTypes').val()

    return JSON.parse(types)
  }
}

const actions = {  
  fetchAll: (store, query) => fetch.actions.fetchAll(store, (api) => api.attributes.fetchAll(query)),
  fetchOne: (store, {id, query}) => fetch.actions.fetchOne(store, (api) => api.attributes.edit(id, query)),  
  fetchAttributesSelect: async ({rootGetters, commit}, {query}) => {
    let api = rootGetters['panelApi']
    let {attributes} = await api.selects.attributes(query)
    commit('setAttributesSelect', attributes)
  }, 
}

const mutations = {
  ...fetch.mutations,
  setAttributesSelect(state, attributes) {
    state.attributesSelect = attributes
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}