import Vue from 'vue'
import Vuex from 'vuex'

import ApiService from "@/common/api.service";

import auth from './auth.module'
import locales from './locales.module'
import attributes from './attributes.module'
import attributeGroups from './attribute-groups.module'
import categories from './categories.module'
import categoriesTree from './categories-tree.module'
import menuItems from './menu-items.module'
import products from './products.module'
import topProgressbar from './top-progressbar.module'

import JwtService from "@/common/jwt.service";
import axios from 'axios'

Vue.use(Vuex)

import Lang from 'lang.js'
import messages from '@/messages'

const lang = new Lang({ 
  messages: messages,
  locale: 'en',
  fallback: 'en' 
})

export default new Vuex.Store({
  namespaced: true,
  watch: {},
	state: {
    lang: lang,
    enums: {
      attributes: {}
    },
    contentLocale: null,
  },
	getters: {
    lang (state) {
      return state.lang
    }, 
    enums (state) {
      return state.enums
    }, 
    attributesEnum(state) {
      let attributes = state.enums.attributes

      return Object.keys(attributes).map(key => ({ value: key, text: attributes[key] }))
    },
    getContentLocale (state) {
      return state.contentLocale
    }, 
    getLocale(state) {
      return state.lang.getLocale()
    },
    getLocales(state) {
      return state.locales
    },
    isSmall() {
      return $( document ).width() < 576
    },
    appName() {
      return $('#appName').val()
    },
    api(state, getters, rootState, rootGetters) {
      let locale = getters['getLocale']
      let contentLocale = getters['getContentLocale']

      return ApiService({
        locale,
        contentLocale,
        prefix: 'ajax',
        token: rootGetters['auth/token']
      })
    },
    panelApi(state, getters, rootState, rootGetters) {
      let locale = getters['getLocale']
      let contentLocale = getters['getContentLocale']

      return ApiService({
        locale,
        contentLocale,
        prefix: 'ajax/panel',
        token: rootGetters['auth/token']
      })
    },
  },
	actions: {
    async panelRequest({commit, rootState, getters, dispatch, rootGetters}, callback) {
      let api = rootGetters['panelApi']
      
      return await callback(api)
    },
    async fetchEnums({commit, rootState, getters, dispatch, rootGetters}) {
      commit('setEnums', await rootGetters.panelApi.enums())
    },
  },
	mutations: {
    setEnums(state, enums) {
      state.enums = enums
    }
  },
  modules: {
    auth, 
    locales, 
    attributes, 
    attributeGroups, 
    categories, 
    categoriesTree,
    menuItems,
    products,
    topProgressbar
  }
})