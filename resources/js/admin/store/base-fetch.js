export default (config = {}) => {
  const allAttribute = config.allAttribute || 'items'
  const oneAttribute = config.oneAttribute || 'item'

  const state = {
    [allAttribute]: {
      data: []
    },
    [oneAttribute]: {}
  }

  const getters = {
    [allAttribute] (state) {
      return state[allAttribute]
    }, 
    [oneAttribute] (state) {
      return state[oneAttribute]
    }, 
  }

  const actions = {
    async fetchAll({commit, rootGetters}, callback) {
      let data = await callback(rootGetters.panelApi)

      commit('setAll', data[allAttribute.camelToSnakeCase()])
    }, 
    async fetchOne({commit, rootGetters}, callback) {
      let data = await callback(rootGetters.panelApi)

      commit('setOne', data[oneAttribute.camelToSnakeCase()])
    },
  }

  const mutations = {
    setAll(state, items) {
      state[allAttribute] = items
    },
    setOne(state, item) {
      state[oneAttribute] = item
    }
  }

  return {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
  }
}