import baseFetch from './base-fetch'

const fetch = baseFetch({
  allAttribute: 'menuItems',
  oneAttribute: 'menuItem',
})

const state = {
  ...fetch.state,
  menuItemsSelect: [],
}

const getters = {
  ...fetch.getters,
  menuItemsSelect(state) {
    return state.menuItemsSelect
  }
}

const actions = {  
  fetchAll: (store, query) => fetch.actions.fetchAll(store, (api) => api.menuItems.fetchAll(query)),  
  fetchOne: (store, {id, query}) => fetch.actions.fetchOne(store, (api) => api.menuItems.edit(id, query)),  
  fetchMenuItemsSelect: async ({rootGetters, commit}, {id, query}) => {
    let api = rootGetters['panelApi']
    let {menu_items} = await api.selects.menuItems(query)
    
    commit('setMenuItemsSelect', menu_items)
  }
}

const mutations = {
  ...fetch.mutations,
  setMenuItemsSelect(state, items) {
    state.menuItemsSelect = items
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}