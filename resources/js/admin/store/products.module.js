import baseFetch from './base-fetch'

const fetch = baseFetch({
  allAttribute: 'products',
  oneAttribute: 'product',
})

const state = {
  ...fetch.state,
  attributes: [],
}

const getters = {
  ...fetch.getters,
  attributes(state) {
    return state.attributes
  },
  productsSelect(state) {
    return state.categoriesSelect
  }
}

const actions = {  
  fetchAll: (store, query) => fetch.actions.fetchAll(store, (api) => api.products.fetchAll(query)),  
  fetchOne: (store, {id, query}) => fetch.actions.fetchOne(store, (api) => api.products.edit(id, query)),    
  fetchAttributes: async ({rootGetters, commit}, {categoryId, query}) => {
    let api = rootGetters['panelApi']
    let {attributes} = await api.attributes.fetchByCategory(categoryId, query)
    commit('setAttributes', attributes)
  },  
  fetchProductsSelect: async ({rootGetters, commit}, {query}) => {
    let api = rootGetters['panelApi']
    let {products} = await api.selects.products(query)
    commit('setProductsSelect', products)
  }
}

const mutations = {
  ...fetch.mutations,
  setAttributes(state, attributes) {
    state.attributes = attributes
  },
  setProductsSelect(state, categories) {
    state.categoriesSelect = categories
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}