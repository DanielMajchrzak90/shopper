require('@/bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router';
import Vuex from 'vuex'
import { BootstrapVue } from 'bootstrap-vue'
import CKEditor from 'ckeditor4-vue'
import vueTopprogress from 'vue-top-progress'
import PerfectScrollbar from 'vue2-perfect-scrollbar'

import store from '@/admin/store/index'

import dayjs from 'dayjs'
import uniqid from 'uniqid'

import '@/admin/components'

import LocaleMixin from '@/mixin/locale';
import ApiMixin from '@/mixin/api';
import RouteLocaleMixin from '@/mixin/route-locale';

import App from '@/admin/App.vue';

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(CKEditor )
Vue.use(vueTopprogress)
Vue.use(PerfectScrollbar)

Vue.mixin(LocaleMixin); 
Vue.mixin(ApiMixin); 
Vue.mixin(RouteLocaleMixin); 

Vue.prototype.$uniqid = uniqid

Vue.prototype.$datetime = (datetime) => {
  return !datetime ? '--:--' : dayjs(datetime).format('HH:mm YYYY-MM-DD')
}

import router from '@/admin/router/router'

const app = new Vue({
  el: '#app',
  components: { App },
  router,
  store,
})