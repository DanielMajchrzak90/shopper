require('@/bootstrap');

import VueRouter from 'vue-router';
import routes from '@/admin/router/routes'
import store from '@/admin/store/index'

_.map(routes, function(route) {
  route.path = '/:content_locale([a-z]{2,3})?' + route.path
  route.path = '/:locale(pl|en)' + route.path
  
  return route
})

const router = new VueRouter({ 
  mode: 'history', 
  routes: routes, 
  linkActiveClass: "active",
});


router.beforeEach(
  async (to, from, next) => {
    var locale = to.params.locale
    var contentLocale = to.params.content_locale

    store.state.lang.setLocale(locale)
    store.state.contentLocale = contentLocale

    const redirect = (payload) => {
      next(payload)

      return true
    }
    
    store.commit('topProgressbar/start')  

    if (
      to.meta.middleware && 
      Array.isArray(to.meta.middleware) && 
      to.meta.middleware.length > 0
    ) {
      var redirected
      var middlewarePromise = new Promise((resolve, reject) => {
        to.meta.middleware.forEach(async (middleware, index) => {
          redirected = await middleware({
            route: to,
            prevRoute: from,
            redirect: redirect,
            store,
          })

          if (index === to.meta.middleware.length -1 || redirected) resolve();
        })
      });

      middlewarePromise.then(() => {
        if (!redirected) {
          next()
        }
      });
    } else {
      next()
    }
  }
)
router.afterEach((to, from) => {
  store.commit('topProgressbar/finish')  
})

export default router