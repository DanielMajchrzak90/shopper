import Start from '@/admin/views/Start'
import Login from '@/admin/views/auth/Login'
import PasswordForgot from '@/admin/views/auth/PasswordForgot'
import PasswordReset from '@/admin/views/auth/PasswordReset'

import Panel from '@/admin/views/Panel'
import Admin from '@/admin/views/Admin'
import Attributes from '@/admin/views/panel/attributes/Index'
import AttributeCreate from '@/admin/views/panel/attributes/Create'
import AttributeEdit from '@/admin/views/panel/attributes/Edit'
import AttributeGroups from '@/admin/views/panel/attribute-groups/Index'
import Categories from '@/admin/views/panel/categories/Index'
import CategoriesCreate from '@/admin/views/panel/categories/Create'
import CategoriesEdit from '@/admin/views/panel/categories/Edit'
import CategoriesTree from '@/admin/views/panel/categories-tree/Index'
import Locales from '@/admin/views/panel/locales/Index'
import MenuItems from '@/admin/views/panel/menu-items/Index'
import MenuItemsCreate from '@/admin/views/panel/menu-items/Create'
import MenuItemsEdit from '@/admin/views/panel/menu-items/Edit'
import Products from '@/admin/views/panel/products/Index'
import ProductsCreate from '@/admin/views/panel/products/Create'
import ProductsEdit from '@/admin/views/panel/products/Edit'

const guestMiddleware = async ({route, redirect, store, prevRoute}) => {
  await store.dispatch('auth/checkAuth')

  if (store.getters['auth/isAuthenticated']) {
    console.log( route.params)
    return redirect({
      name: 'panel',
      params: route.params
    })
  }
}

const authMiddleware = async ({route, redirect, store, prevRoute}) => {
  await store.dispatch('auth/checkAuth')

  if (!store.getters['auth/isAuthenticated']) {
    console.log( route.params)
    return redirect({
      name: 'login',
      params: route.params
    })
  }
}

const enumMiddleware = async ({route, redirect, store}) => {
  await store.dispatch('fetchEnums')
}

const localesMiddleware = async ({route, redirect, store}) => {
  await store.dispatch('locales/fetchAll', route.query)
}

const routes = [
  {
    path: '/',
    name: 'start',
    redirect: { name: 'admin' }
  },
  {
    path: '/admin',
    name: 'admin',
    component: Admin,
    redirect: { name: 'panel' },
    children: [
      {
        path: 'login',
        name: 'login',
        component: Login,
        meta: {
          layout: 'auth-layout',
          middleware: [guestMiddleware],
          title: 'Logowanie',
        },
      },
      {
        path: 'password-forgot',
        name: 'password-forgot',
        component: PasswordForgot,
        meta: {
          layout: 'auth-layout',
          middleware: [guestMiddleware],
          title: 'Przypomnienie hasła',
        }
      }, 
      {
        path: 'password-reset',
        name: 'password-reset',
        component: PasswordReset,
        meta: {
          layout: 'auth-layout',
          middleware: [guestMiddleware],
          title: 'Reset hasła',
        }
      },
      {
        path: 'panel',
        name: 'panel',
        component: Panel,
        meta: {
          layout: 'panel-layout',
          middleware: [authMiddleware],
        },
        children: [
          {
            path: 'locales',
            name: 'locales',
            component: Locales,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware],
              title: (lang) => {
                return lang.get('messages.locales')
              },
            },
          },
          {
            path: 'attributes',
            name: 'attributes',
            component: Attributes,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.attributes')
              },
            },
          },
          {
            path: 'attributes/create',
            name: 'attributes-create',
            component: AttributeCreate,
            meta: {
              layout: 'panel-layout',
              middleware: [
                authMiddleware, 
                enumMiddleware, 
                localesMiddleware
              ],
              title: (lang) => {
                return lang.get('messages.attribute_create')
              },
            },
          },
          {
            path: 'attributes/:id/edit',
            name: 'attributes-edit',
            component: AttributeEdit,
            meta: {
              layout: 'panel-layout',
              middleware: [
                authMiddleware, 
                enumMiddleware, 
                localesMiddleware
              ],
              title: (lang) => {
                return lang.get('messages.item_edit', {item: lang.get('messages.attribute')})
              },
            },
          },      
          {
            path: 'attribute-groups',
            name: 'attribute-groups',
            component: AttributeGroups,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.attribute_groups')
              },
            },
          },  
          {
            path: 'categories',
            name: 'categories',
            component: Categories,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.categories')
              },
            },
          },
          {
            path: 'categories/create',
            name: 'categories-create',
            component: CategoriesCreate,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.item_create', {
                  item: lang.get('messages.category')
                })
              },
            },
          },
          {
            path: 'categories/:id/edit',
            name: 'categories-edit',
            component: CategoriesEdit,
            meta: {
              layout: 'panel-layout',
              middleware: [
                authMiddleware, 
                enumMiddleware, 
                localesMiddleware
              ],
              title: (lang) => {
                return lang.get('messages.item_edit', {item: lang.get('messages.category')})
              },
            },
          },  
          {
            path: 'categories-tree',
            name: 'categories-tree',
            component: CategoriesTree,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.categories_tree')
              },
            },
          },
          {
            path: 'menu-items',
            name: 'menu-items',
            component: MenuItems,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.menu_items')
              },
            },
          },
          {
            path: 'menu-items/create',
            name: 'menu-items-create',
            component: MenuItemsCreate,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.item_create', {item: lang.get('messages.menu_item')})
              },
            },
          },
          {
            path: 'menu-items/:id/edit',
            name: 'menu-items-edit',
            component: MenuItemsEdit,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.item_edit', {item: lang.get('messages.menu_item')})
              },
            },
          },
          {
            path: 'products',
            name: 'products',
            component: Products,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.products')
              },
            },
          },
          {
            path: 'products/create',
            name: 'products-create',
            component: ProductsCreate,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.item_create', {item: lang.get('messages.product')})
              },
            },
          },
          {
            path: 'products/:id/edit',
            name: 'products-edit',
            component: ProductsEdit,
            meta: {
              layout: 'panel-layout',
              middleware: [authMiddleware, localesMiddleware],
              title: (lang) => {
                return lang.get('messages.item_edit', {item: lang.get('messages.product')})
              },
            },
          },
        ]
      },
    ]
  },
]

export default routes