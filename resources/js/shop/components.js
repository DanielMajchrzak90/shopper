import Vue from 'vue'

import Layout from '@/layouts/ShopLayout'
import PageFooter from '@/components/Footer'

import MenuItems from '@/shop/components/MenuItems'
import CategoriesTree from '@/shop/components/CategoriesTree'
import Sidebar from '@/shop/components/Sidebar'
import ProductCard from '@/shop/components/ProductCard'
import BindngProductThumb from '@/shop/components/BindngProductThumb'

import AttributesForm from '@/shop/components/attributes/Form'
import ColorField from '@/shop/components/attributes/ColorField'
import SelectField from '@/shop/components/attributes/SelectField'
import BoolField from '@/shop/components/attributes/BoolField'
import NumberField from '@/shop/components/attributes/NumberField'
import RadioField from '@/shop/components/attributes/RadioField'

Vue.component('layout', Layout)
Vue.component('page-footer', PageFooter)

Vue.component('menu-items', MenuItems)
Vue.component('categories-tree', CategoriesTree)
Vue.component('sidebar', Sidebar)
Vue.component('product-card', ProductCard)
Vue.component('binding-product-thumb', BindngProductThumb)

Vue.component('attributes-form', AttributesForm)
Vue.component('color-field', ColorField)
Vue.component('select-field', SelectField)
Vue.component('number-field', NumberField)
Vue.component('bool-field', BoolField)
Vue.component('radio-field', RadioField)
