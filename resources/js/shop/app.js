require('@/bootstrap');

import Vue from 'vue'
import Vuex from 'vuex'
import { BootstrapVue } from 'bootstrap-vue'
import CKEditor from 'ckeditor4-vue'
import vueTopprogress from 'vue-top-progress'
import PerfectScrollbar from 'vue2-perfect-scrollbar'

import store from '@/shop/store/index'

import dayjs from 'dayjs'
import uniqid from 'uniqid'

Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(CKEditor )
Vue.use(vueTopprogress)
Vue.use(PerfectScrollbar)

Vue.mixin(LocaleMixin); 
Vue.mixin(ApiMixin); 

import '@/shop/components'

import LocaleMixin from '@/mixin/locale';
import ApiMixin from '@/mixin/api';

Vue.prototype.$uniqid = uniqid

import App from '@/shop/App.vue';

const app = new Vue({
  el: '#app',
  components: { App },
  store,
  mounted() {
    $('.dropdown.click-outside .dropdown-menu').click(function(event){
      event.stopPropagation();
    });
    $('#startLoader').hide()
  }
})