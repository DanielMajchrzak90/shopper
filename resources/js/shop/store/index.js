import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import Lang from 'lang.js'
import messages from '@/messages'

const lang = new Lang({ 
  messages: messages,
  locale: 'en',
  fallback: 'en' 
})

export default new Vuex.Store({
  namespaced: true,
  watch: {},
	state: {
    lang: lang,
  },
	getters: {
    lang (state) {
      return state.lang
    }, 
    appName() {
      return $('#appName').val()
    },
  },
	actions: {

  },
	mutations: {

  },
  modules: {
    
  }
})