import JwtService from "@/common/jwt.service";
import axios from 'axios'

export default ({locale, contentLocale, prefix, token}) => {
  let api = axios.create({});
  api.defaults.baseURL = '/'+locale+(contentLocale ? ('/'+contentLocale+'/') : '/')+prefix
  api.defaults.headers.common["Authorization"] = 'Bearer ' + token;

  const get = async (path, query) => {
    let res = await api.get(path, {
      params: query
    })

    return res.data
  }
  const post = async (path, payload) => {
    let config = {}
    if (payload instanceof FormData) {
      config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    } 
    
    let res = await api.post(path, payload, config)

    return res.data
  }
  const patch = async (path, payload) => {
    let method = 'PATCH'
    let config = {}
    if (payload instanceof FormData) {
      payload.append('_method', method)
      config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    } else {
      payload._method = method
    }

    let res = await api.post(path, payload, config)
    
    return res.data
  }
  const destroy = async (path, payload) => {
    let res = await api.delete(path, payload)
    
    return res.data
  }

  const crud = (path) => {
    return {
      fetchAll: (query) => get(path, query),
      create: (payload) => post(path, payload),
      edit: (id, query) => get(path+'/'+id, query),
      update: (id, payload) => patch(path+'/'+id, payload),
      destroy: (id) => destroy(path+'/'+id),
    }
  }

  return {
    attributes: {
      ...crud('/attributes'),
      fetchByCategory: (id, query) => get('/attributes-by-category/'+id, query)
    },
    attributeGroups: crud('/attribute-groups'),
    locales: crud('/locales'),
    categories: crud('/categories'),
    menuItems: crud('/menu-items'),
    products: crud('/products'),
    categoriesTree: {
      fetch: (query) => get('/categories-tree', query),
      update: (payload) => patch('/categories-tree', payload),
    },
    enums: () => get('/enums'),
    auth: {
      login: (payload) => post('/auth/login', payload),
      register: (payload) => post('/auth/register', payload),
      forgotPassword: (payload) => post('/auth/forgot-password', payload),
      resetPassword: (payload) => post('/auth/reset-password', payload),
      checkAuth: () => post('/auth/me'),
      logout: () => post('/auth/logout'),
    },
    selects: {
      attributes: (query) => get('/selects/attributes', query),
      attributeGroups: (query) => get('/selects/attribute-groups', query),
      menuItems: (query) => get('/selects/menu-items', query),
      categories: (query, id = null) => get('/selects/categories/'+(id || ''), query),
      products: (query) => get('/selects/products', query),
    }
  }
}
