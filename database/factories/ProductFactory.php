<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

class ProductFactory extends BaseFactory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => Category::whereIsLeaf()->inRandomOrder()->first()->id,
            'sku' => uniqid(),
            'price' => $this->faker->randomNumber(2),
            'quantity' => $this->faker->numberBetween(20, 300),
            'pl' => [
                'name' => $this->plFaker->sentence(3),
                'slug' => $this->plFaker->unique()->slug,
                'description' => $this->plFaker->text(1000),
            ],
            'en' => [
                'name' => $this->faker->sentence(3),
                'slug' => $this->faker->unique()->slug,
                'description' => $this->faker->text(1000),
            ]
        ];
    }
}
