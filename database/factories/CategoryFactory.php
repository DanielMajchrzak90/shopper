<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

class CategoryFactory extends BaseFactory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pl' => [
                'name' => $this->plFaker->sentence(5),
                'slug' => $this->plFaker->slug,
            ],
            'en' => [
                'name' => $this->faker->sentence(5),
                'slug' => $this->faker->slug,
            ],
        ];
    }
}
