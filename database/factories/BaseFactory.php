<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

abstract class BaseFactory extends Factory
{
    protected $plFaker;

    public function __construct($count = null,
                                ?Collection $states = null,
                                ?Collection $has = null,
                                ?Collection $for = null,
                                ?Collection $afterMaking = null,
                                ?Collection $afterCreating = null,
                                $connection = null)
    {
        parent::__construct($count, $states, $has, $afterMaking, $afterCreating, $connection);
        $this->plFaker = \Faker\Factory::create('pl_PL');
    }
}
