<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MenuItemColFactory extends BaseFactory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pl' => [
                'name' => $this->plFaker->sentence(4),
            ],
            'en' => [
                'name' => $this->faker->sentence(4),
            ],
        ];
    }
}
