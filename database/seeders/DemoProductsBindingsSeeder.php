<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductBinding;

class DemoProductsBindingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductBinding::query()->delete();
        $products = Product::noVariantsFilter()->get();

        $products->chunk(5)
            ->each(function($products) {
                $products->first()->saveRelatedProducts($products->skip(1)->pluck('id')->toArray());
            });
    }
}
