<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use ACME\DTOS\Base\ProductVariantDTO;
use ACME\DTOS\Base\ProductAttributeValueDTO;

class DemoProductsVariantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        Product::noVariantsFilter()
            ->get()
            ->each(function($product) use ($faker) {
                $variants = [];

                for ($i = 0; $i < 5; $i++) { 
                    $variantDTO = (new ProductVariantDTO)
                        ->setPrice($faker->randomNumber(2))
                        ->setQuantity($faker->numberBetween(20, 300));

                    foreach ($product->category->variantAttributes() as $attribute) {
                        $type = $attribute->class_type;
                        $type->setAttribute($attribute);

                        $variantDTO->addAttribute($type->fakeDTO()->setAttributeId($attribute->id));
                    }

                    $variants[] = $variantDTO;
                }

                $product->saveVariants($variants);
            });
    }
}
