<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Events\ResourceChanged;

class DemoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DemoUsersSeeder::class);
        $this->call(DemoAttributesSeeder::class);
        $this->call(DemoAttributeGroupsSeeder::class);
        $this->call(DemoCategoriesSeeder::class);
        $this->call(DemoMenuItemsSeeder::class);
        $this->call(DemoProductsSeeder::class);
        $this->call(DemoProductsVariantsSeeder::class);
        $this->call(DemoProductsBindingsSeeder::class);
        
        ResourceChanged::dispatch();
    }
}
