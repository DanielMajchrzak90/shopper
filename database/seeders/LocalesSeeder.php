<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Locale;

class LocalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = [
            [
                'code' => 'pl',
                'name' => 'Polski'
            ],
            [
                'code' => 'en',
                'name' => 'English'
            ]
        ];

        foreach ($locales as $locale) {
            Locale::create($locale);
        }
    }
}
