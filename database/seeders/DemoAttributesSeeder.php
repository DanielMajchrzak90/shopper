<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Arr;
use DB;
use App\Models\Attribute;
use ACME\DTOS\Base\AttributeDTO;
use ACME\DTOS\Base\AttributeTranslationDTO;
use ACME\DTOS\Base\AttributeOptionDTO;
use ACME\DTOS\Base\AttributeOptionTranslationDTO;

class DemoAttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $plFaker = \Faker\Factory::create('pl_PL');
        $colors = [];

        for ($i=0; $i < 10; $i++) { 
            $colors[] = [
                'additional' => [
                    'color' => $faker->hexcolor,
                ],
                'en' => [
                    'name' => $faker->colorName,
                ],
                'pl' => [
                    'name' => $plFaker->colorName,
                ],
            ];
        }

        $sizes = [];

        foreach ([
            'XXL',
            'XL',
            'L',
            'M',
            'S',
        ] as $size) {
            $sizes[] = [
                'pl' => [
                    'name' => $size
                ],
                'en' => [
                    'name' => $size
                ],
            ];
        }

        $attributes = [
            [
                'type' => \ACME\AttributeTypes\ColorType::class,
                'name' => 'color',
                'pl' => [
                    'label' => 'Kolor',
                ],
                'en' => [
                    'label' => 'Color',
                ],
                'options' => $colors,
            ],
            [
                'type' => \ACME\AttributeTypes\SelectType::class,
                'name' => 'size',
                'pl' => [
                    'label' => 'Rozmiar',
                ],
                'en' => [
                    'label' => 'Size',
                ],
                'options' => $sizes,
            ],
            [
                'type' => \ACME\AttributeTypes\NumberType::class,
                'name' => 'length',
                'pl' => [
                    'label' => 'Długość',
                ],
                'en' => [
                    'label' => 'Length',
                ],
            ],
            [
                'type' => \ACME\AttributeTypes\NumberType::class,
                'name' => 'wide',
                'pl' => [
                    'label' => 'Szerokość',
                ],
                'en' => [
                    'label' => 'Wide',
                ],
            ],
            [
                'type' => \ACME\AttributeTypes\TextType::class,
                'name' => 'additional_desc',
                'pl' => [
                    'label' => 'Dodatkowy opis',
                ],
                'en' => [
                    'label' => 'Additional description',
                ],
            ],
        ];

        DB::transaction(function() use ($attributes) {
            Attribute::query()->delete();

            foreach ($attributes as $attribute) {
                $type = Arr::get($attribute, 'type');

                $dto = new AttributeDTO();

                $dto
                    ->setType($type)
                    ->setName(Arr::get($attribute, 'name'))
                    ->setIsVariant(Arr::get($attribute, 'name'))
                    ->addTranslation(
                        (new AttributeTranslationDTO)
                            ->setLocale('pl')
                            ->setLabel(Arr::get($attribute, 'pl.label'))
                    )
                    ->addTranslation(
                        (new AttributeTranslationDTO)
                            ->setLocale('en')
                            ->setLabel(Arr::get($attribute, 'en.label'))
                    );

                foreach (Arr::get($attribute, 'options', []) as $option) {
                    $dto->addOption(
                        (new AttributeOptionDTO)
                            ->setAdditional(Arr::get($option, 'additional'))
                            ->addTranslation(
                                (new AttributeOptionTranslationDTO)
                                    ->setLocale('pl')
                                    ->setName(Arr::get($option, 'pl.name'))
                            ) 
                            ->addTranslation(
                                (new AttributeOptionTranslationDTO)
                                    ->setLocale('en')
                                    ->setName(Arr::get($option, 'en.name'))
                            )
                    );
                }

                (new $type)->save($dto);
            }
        });
    }
}
