<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AttributeGroup;
use App\Models\Attribute;
use DB;

class DemoAttributeGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function() {
            AttributeGroup::query()->delete();

            $group = AttributeGroup::create([
                'name' => 'Basic',
            ]);

            $group->attributes()->sync(Attribute::get()->pluck('id')->toArray());
        });
    }
}
