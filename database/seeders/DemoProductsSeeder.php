<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Product;
use ACME\DTOS\Base\ProductAttributeValueDTO;

class DemoProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::query()->delete();

        Category::defaultOrder()->whereIsLeaf()
            ->take(20)
            ->get()
            ->each(function($category) {
                Product::factory(50)->create([
                    'category_id' => $category->id
                ])->each(function($product) use ($category) {
                    $attributes = $category->attributes();

                    if ($attributes->count() > 0) {
                        $attributeDtos = $attributes->map(function($attribute) {
                            $type = $attribute->class_type;
                            $type->setAttribute($attribute);

                            return $type->fakeDTO()->setAttributeId($attribute->id);
                        });

                        $product->saveAttributes($attributeDtos->toArray());
                    }
                });
            });
    }
}
