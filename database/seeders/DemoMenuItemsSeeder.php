<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\MenuItem;
use App\Models\MenuItemCol;
use ACME\DTOS\MultipleTranslationDTO\MenuItemDTO;

class DemoMenuItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuItem::query()->delete();

        $categories = Category::whereNull('parent_id')->get();

        $categories->take(5)->each(function($category) { 
            $menuItem = MenuItem::create([
                'category_id' => $category->id
            ]);
            $cols = MenuItemCol::factory()
                ->count(3)
                ->create([
                    'item_id' => $menuItem->id
                ]); 

            $cols->each(function($col) use ($category) {
                $category->descendants->random(7)->each(function($colCategory) use ($col) {                    
                    $colMenuItem = MenuItem::create([
                        'category_id' => $colCategory->id
                    ]);
                    $col->items()->attach($colMenuItem->id);
                });
            });          
        });
    }
}
