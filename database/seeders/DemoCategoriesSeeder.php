<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\AttributeGroup;

class DemoCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        Category::query()->delete();
        
        Category::factory()
            ->count(10)
            ->has(
                Category::factory()
                    ->count(10)
                    ->has(
                        Category::factory()
                            ->count(5)
                            ->has(
                                Category::factory()
                                    ->count(5),
                                'children'
                            ),
                        'children'
                    ),
                'children'
            )
            ->create();

        $group = AttributeGroup::first();
        $leafs = Category::whereIsLeaf()->get();
        $leafs->each(function($leaf) use ($group) {
            $leaf->update([
                'group_id' => $group->id
            ]);
        });
    }
}
