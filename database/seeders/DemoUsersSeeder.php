<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DemoUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->delete();
        User::factory()->create([
            'email' => 'danielmajchrzak90@gmail.com'
        ]);
        User::factory(10)->create();
    }
}
