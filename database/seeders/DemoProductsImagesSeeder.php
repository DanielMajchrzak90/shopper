<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use ACME\DTOS\Base\ProductImageDTO;
use Storage;
use App\Models\Product;
use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Filesystem\Filesystem;

class DemoProductsImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $seedDisk = Storage::disk('seed_images');
        $dirs = $seedDisk->directories();
        $dirs = collect($dirs);
        $file = new Filesystem;

        foreach (['images', 'thumbnails'] as $name) {
            $disk = Storage::disk($name);
            $files =   $disk->allFiles();
            $disk->delete($files);
        }
        
        Product::noVariantsFilter()
            ->get()
            ->each(function($product) use ($faker, $seedDisk, $dirs) {
                $dir = $dirs->random();
                $files = $seedDisk->files($dir);
                $images = [];

                foreach ($files as $file) {
                    $path = $seedDisk->path($file);
                    $images[] = new File($path);
                }

                $product->saveImages($images);
            });
    }
}
