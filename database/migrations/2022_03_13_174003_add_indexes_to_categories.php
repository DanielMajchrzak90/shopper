<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->index('_lft');
            $table->index('_rgt');
            // $table->index('parent_id');
            $table->index(['_lft', '_rgt']);
            $table->index(['_lft', '_rgt', 'parent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropIndex(['_lft']);
            $table->dropIndex(['_rgt']);
            // $table->dropIndex(['_lft', '_rgt']);
            // $table->dropIndex(['_lft', '_rgt', 'parent_id']);
        });
    }
}
