<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAttributeOptionValueInProductAttributeValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_attribute_values', function (Blueprint $table) {
            $table->dropForeign('product_attribute_attribute_option_value_foreign');
            $table->renameColumn('attribute_option_value', 'option_value');
            $table->foreign('option_value')
                ->references('id')
                ->on('attribute_options')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_attribute_values', function (Blueprint $table) {
            $table->foreignId('attribute_option_value')->nullable();

            $table->foreign('attribute_option_value')
                ->references('id')
                ->on('attribute_options')
                ->onDelete('cascade');
        });
    }
}
