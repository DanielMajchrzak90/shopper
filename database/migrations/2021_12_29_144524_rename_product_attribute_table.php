<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameProductAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('product_attribute', 'product_attribute_values');
        Schema::rename('product_attribute_translations', 'product_attribute_value_translations');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('product_attribute_values', 'product_attribute');
        Schema::rename('product_attribute_value_translations', 'product_attribute_translations');
    }
}
