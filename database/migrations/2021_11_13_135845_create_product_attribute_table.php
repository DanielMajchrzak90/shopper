<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->nullable()->index();

            $table->foreignId('product_id');
            $table->foreignId('attribute_id');
            $table->foreignId('attribute_option_value')->nullable();
            $table->text('text_value');
            $table->boolean('bool_value');

            $table->unique(['attribute_id','product_id']);

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
            
            $table->foreign('attribute_id')
                ->references('id')
                ->on('attributes')
                ->onDelete('cascade');

            $table->foreign('attribute_option_value')
                ->references('id')
                ->on('attribute_options')
                ->onDelete('cascade');

            $table->foreign('locale')
                ->references('code')
                ->on('locales')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute');
    }
}
