<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributeTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pa_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['pa_id', 'locale']);
            $table->text('text_value');

            $table->foreign('pa_id')
                ->references('id')
                ->on('product_attribute')
                ->onDelete('cascade');
            $table->foreign('locale')
                ->references('code')
                ->on('locales')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute_translations');
    }
}
