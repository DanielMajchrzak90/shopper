<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemColPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_item_col_pivot', function (Blueprint $table) {
            $table->id();
            $table->foreignId('item_id')->unsigned();
            $table->foreignId('col_id')->unsigned();

            $table->unique(['item_id', 'col_id']);

            $table->foreign('item_id')
                ->references('id')
                ->on('menu_items')
                ->onDelete('cascade');

            $table->foreign('col_id')
                ->references('id')
                ->on('menu_item_cols')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_item_col_pivot');
    }
}
