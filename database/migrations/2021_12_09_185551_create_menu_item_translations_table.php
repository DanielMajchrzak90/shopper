<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_item_translations', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->string('slug')->nullable();
            $table->foreignId('item_id');   
            $table->string('locale')->index();
            $table->unique(['item_id', 'slug', 'locale']);
            $table->foreign('item_id')
                ->references('id')
                ->on('menu_items')
                ->onDelete('cascade');
            $table->foreign('locale')
                ->references('code')
                ->on('locales')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_item_translations');
    }
}
