<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemColTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_item_col_translations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('col_id');
            $table->string('locale')->index();
            $table->unique(['col_id', 'locale']);
            $table->foreign('col_id')
                ->references('id')
                ->on('menu_item_cols')
                ->onDelete('cascade');
            $table->foreign('locale')
                ->references('code')
                ->on('locales')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_item_col_translations');
    }
}
